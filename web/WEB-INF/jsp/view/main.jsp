<%--@elvariable id="shows" type="java.util.List<dto.ShowDto>"--%>
<%--@elvariable id="articles" type="java.util.List<dto.ArticleDto>"--%>
<%--@elvariable id="presenters" type="java.util.List<dto.PresenterDto>"--%>


<template:main htmlTitle="hello" >
        <jsp:attribute name="activeLink">main</jsp:attribute>
        <jsp:body>
            <div id="red" class="block red lighten-1">
                <nav>
                    <div class="nav-wrapper red">
                        <div class="container">
                            <a href="#" class="brand-logo"><spring:message code="title.shows" /></a>
                        </div>
                    </div>
                </nav>

                <div class="container">
                <div class="row">

                    <c:forEach items="${shows}" var="show">

                        <div class="col-sm-6 col-md-4 col-lg-3">

                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="${show.imageUrl}">
                                </div>
                                    <div class="card-content">
                                            <span class="card-title flow-text activator grey-text text-darken-4">
                                                <a href="/show/?showId=${show.id}">${show.name}</a><i class="material-icons right">more_vert</i></span>
                                    </div>
                                    <div class="card-reveal">
                                    <span class="card-title flow-text grey-text text-darken-4">${show.name}<i class="material-icons right">close</i></span>
                                    <p>${show.description}</p>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                </div>

            </div>

            <div id="blue" class="block blue lighten-1">
                <nav>
                    <div class="nav-wrapper blue">
                        <div class="container">
                            <a href="#" class="brand-logo"><spring:message code="title.presenters" /></a>
                        </div>
                    </div>
                </nav>
                <div class="container">
                <div class="row">
                    <c:forEach items="${presenters}" var="presenter">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img class="responsive-img" src="${presenter.imageUrl}">
                                </div>
                                <div class="card-content">
                                    <span class="card-title activator grey-text text-darken-4">${presenter.name}<i class="material-icons right">more_vert</i></span>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4">${presenter.name}<i class="material-icons right">close</i></span>
                                    <p>${presenter.dob}</p>
                                    <p>${presenter.description}</p>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            </div>

            <div id="green" class="block green lighten-1">
                <nav>
                    <div class="nav-wrapper green">
                        <div class="container">
                            <a href="#" class="brand-logo"><spring:message code="title.articles" /></a>
                        </div>
                    </div>
                </nav>
                <div class="container">
                <div class="row">
                    <c:forEach items="${articles}" var="article">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="${article.imageUrl}">
                                </div>
                                <div class="card-content">
                                    <span class="card-title activator grey-text text-darken-4">${article.header}<i class="material-icons right">more_vert</i></span>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4">${article.header}<i class="material-icons right">close</i></span>
                                    <p>${article.text}</p>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            </div>

        </jsp:body>

    </template:main>