<%--@elvariable id="user" type="dto.UserDto"--%>
<html>
<body>
<template:main htmlTitle="account" >
    <jsp:attribute name="activeLink">account</jsp:attribute>
    <jsp:body>
<div class="block red lighten-4">
        <c:if test="${!sessionScope.containsKey('user')}">
            <div class="container" style="height: 100%">
                <h3>У вас недостаточно прав для просмотра этой страницы, пожалуйста войдите в систему. <a href="/main/">Вернуться на главную</a></h3>
            </div>
        </c:if>

        <c:if test="${sessionScope.containsKey('user')}">
            <div class="container" style="height: 100%">
                <h3>Добрый день ${user.name}!</h3>
                <div class="row">
                    <img class="myImg avatar" src="${user.imageUrl}"/>
                    <div class="col s12 m6">
                        <p class="flow-text " >Раздел находится в стадии разработки, в дальнейшем здесь
                            будут отображаться ваши любимые телешоу и подписки.
                            Для предложений и жалоб свяжитесь с разработчиком.</p>
                    </div>
                </div>
            </div>
        </c:if>
</div>
    </jsp:body>
</template:main>
</body>
</html>