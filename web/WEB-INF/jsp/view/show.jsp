<%--@elvariable id="show" type="dto.ShowDto"--%>

<html>
<body>
<template:main htmlTitle="show" >
    <jsp:attribute name="activeLink">show</jsp:attribute>
    <jsp:body>
        <div class="block red lighten-4">
        <div class="container">
            <h3>${show.name}</h3>

            <div class="row">
                    <img class="myImg" src="${show.imageUrl}"/>
                <div class="col s12 m6">
                            <p class="flow-text " >${show.description}</p>
                </div>
            </div>

            <c:if test="${show.getClass().getName().equals('dto.SeriesDto') || show.getClass().getName().equals('dto.AuthorShowDto')}">
                <h4>Episodes</h4>
            <div class="row">
                <c:forEach items="${show.episodes}" var="episode">
                <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src="${episode.imageUrl}">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">s${episode.season} e${episode.episodeNumber}<i class="material-icons right">more_vert</i></span>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">${episode.name}<i class="material-icons right">close</i></span>
                        <p>${episode.description}</p>
                    </div>
                </div>
                </div>
            </c:forEach>
                </div>
            </c:if>

            <c:if test="${show.getClass().getName().equals('dto.AuthorShowDto')}">
            <h4>Presenters</h4>
            <div class="row">
                <c:forEach items="${show.presenters}" var="presenter">
                <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="${presenter.imageUrl}">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">${presenter.name}<i class="material-icons right">more_vert</i></span>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4">${presenter.name}<i class="material-icons right">close</i></span>
                    <p>${presenter.dob}</p>
                    <p>${presenter.description}</p>
                </div>
            </div>
        </div>
                </c:forEach>
            </div>
            </c:if>
        </div>
        </div>
    </jsp:body>

</template:main>
</body>
</html>