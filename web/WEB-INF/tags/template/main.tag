<%@ tag pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="htmlTitle" type="java.lang.String" rtexprvalue="true" required="true" %>
<%@ attribute name="headContent" fragment="true" required="false" %>
<%@ attribute name="activeLink" type="java.lang.String" rtexprvalue="true" required="false"  %>

<%@ include file="/WEB-INF/jsp/base.jspf" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/resources/css/external/bootstrap.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/resources/css/external/materialize.css" />" />
    <link rel="stylesheet" href="<c:url value="/resources/css/external/materialize.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/resources/css/external/material-icons.css" />" />
    <link rel="stylesheet" href="<c:url value="/resources/css/external/bootstrapValidator.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/resources/css/external/languages.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/resources/css/mystyle.css" />" />

    <script src="<c:url value="/resources/js/external/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/resources/js/external/materialize.min.js" />"></script>
    <script src="<c:url value="/resources/js/external/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/main.js" />"></script>

    <jsp:invoke fragment="headContent" />

</head>
<body>
<div class="navbar-fixed">

    <nav>
    <div class="nav-wrapper">
        <a href="/main/" class="brand-logo">TV Channel</a>
        <a data-activates="side-nav" class="button-collapse" href="#side-nav"><i class="material-icons">menu</i></a>
        <ul id="main-nav" class="right hide-on-med-and-down">

            <li><a class="btn-floating btn-large waves-effect waves-light white" id="schedule-btn"><img class="myLogo" src="\resources\images\tv.jpg"/></a></li>
            <li class="dropdown">
                <c:set var="languageCode" value="${pageContext.response.locale.language}" />
                <a class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="lang-lg" lang="${languageCode}"></span><span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a type="button" onclick="setLocale('en');"><span class="lang-sm lang-lbl-full" lang="en"></span></a></li>
                    <li><a type="button" onclick="setLocale('ru');"><span class="lang-sm lang-lbl-full" lang="ru"></span></a></li>
                    <li><a type="button" onclick="setLocale('uk');"><span class="lang-sm lang-lbl-full" lang="uk"></span></a></li>
                </ul>
            </li>
            <c:if test="${!sessionScope.containsKey('user')}">
                <li><a id="registration-btn" type="button"><spring:message code="title.registration" /></a></li>
                <li><a id="login-btn" type="button"><spring:message code="title.login" /></a></li>
            </c:if>

            <c:if test="${sessionScope.containsKey('user')}">
                <li><a id="log-out-btn" type="button"><spring:message code="title.logout" /></a></li>
                <li><a id="my-acc-btn" href="/account/myAccount" type="button"><img src="${user.imageUrl}" class="ava circle"></a></li>
            </c:if>
        </ul>

        <ul class="side-nav" id="side-nav">
            <li><a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">TV</i></a></li>
            <li><a class="dropdown-button" data-activates="language-dropdown-side-nav" data-beloworigin = "true" >language<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a><spring:message code="title.registration" /></a></li>
            <li><a><spring:message code="title.login" /></a></li>
        </ul>
    </div>
</nav>
</div>
    <ul id="language-dropdown-side-nav" class="dropdown-content">
        <li>en</li>
        <li>ru</li>
        <li>uk</li>
    </ul>


<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" ><spring:message code="title.login" /></h4>
            </div>
            <div class="modal-body">
            <div class="input-field">
                <input id="input-login" type="text">
                <label for="input-login">Login</label>
            </div>
            <div class="input-field">
                <input id="input-password" type="password">
                <label for="input-password">Password</label>
            </div>
        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-light btn green" id="submit-login"><i class="material-icons left">done</i>Sign in</a>
            <a class="waves-effect waves-light btn red" id="cancel-login" data-dismiss="modal"><i class="material-icons left">cancel</i>Cancel</a>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="tv-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" ><spring:message code="title.tvprogram" /></h4>
            </div>
            <div class="modal-body">
                <c:forEach items="${sessionScope.get('schedule')}" var="item">
                    <p class="text-center">${item.date}</p>
                    <table class="highlight">

                        <tbody>

                            <tr>

                                <td class="left-align">
                                    <a id="schedule-item" href="/show/?showId=${item.showId}">${item.showName}</a></td>

                                <td class="right-align">${item.startTime} - ${item.endTime}</td>
                            </tr>

                        </tbody>
                    </table>
                    <hr/>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="registration-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Registration</h4>
        </div>
        <div class="modal-body">

            <form id="registration-form">

            <div class="input-field">
                <input id="reg-input-login" name="inputLogin" type="text" class="validate" pattern="[A-ZА-ЯЁЇІa-zа-яёїіє' -`0-9_]*">
                <label  data-error="Only letters and symbols [-`_'] are available" data-success="Ok :)" for="reg-input-login">Login</label>
                <span class="error" id="login-error"> </span>
            </div>

            <div class="input-field">
                <input id="reg-input-email" name="inputEmail" type="email" class="validate">
                <label data-error="Enter valid email" data-success="Ok :)" for="reg-input-login">E-mail</label>
                <span class="error" id="email-error"> </span>
            </div>

            <div class="input-field">
                <input id="reg-input-password" name="inputPassword" type="password" class="validate" pattern=".{6,25}">
                <label data-error="Your password must be longer than 6 symbols" data-success="Ok :)" for="reg-input-password">Password</label>
                <span class="error" id="password-error"> </span>
            </div>

            <div class="input-field">
                <input id="reg-input-password-confirm" name="inputPasswordConfirm" type="password" class="validate">
                <label for="reg-input-password">Confirm password</label>
                <span class="error" id="password-confirm-error"> </span>
            </div>

            <div class="input-field">
                <input id="reg-input-name" name="inputName" type="text" class="validate" pattern="[A-ZА-ЯЁЇІa-zа-яёїіє' -`]*">
                <label for="reg-input-name">Name</label>
                <span class="error" id="name-error"> </span>
            </div>

            </form>
        </div>
        <div class="modal-footer">
            <a class="waves-effect waves-light btn green" id="submit-registration"><i class="material-icons left">done</i>Sign in</a>
            <a class="waves-effect waves-light btn red" id="cancel-registration" data-dismiss="modal"><i class="material-icons left">cancel</i>Cancel</a>
        </div>
    </div>
    </div>
</div>

<jsp:doBody />

<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            (c) 2016 <spring:message code="title.author" />
            <a class="grey-text text-lighten-4 right" href="https://vk.com/sashasashasashenka"><spring:message code="title.contactdev" /></a>
        </div>
    </div>
</footer>
</body>