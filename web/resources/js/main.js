
$( document ).ready(function() {
    $(".dropdown-button").dropdown();

    $(".button-collapse").sideNav();

    $("#login-btn").on("click", function () {
        $('#login-modal').modal('show');
    })

    $("#schedule-btn").on("click", function () {
        $('#tv-modal').modal('show');
    })

    $("#log-out-btn").on("click", function () {
        logout();
    })

    $("#registration-btn").on("click", function () {
        $('#registration-modal').modal('show');
    })

    $("#submit-login").on("click", function () {
        login($("#input-login").val(), $("#input-password").val());

    })

    $("#submit-registration").on("click", function () {

        registration($("#reg-input-login").val(), $("#reg-input-password").val(), $("#reg-input-name").val(), $("#reg-input-email").val());

    })
})
function login(login, password) {
        $.ajax({
            url:"/account/login"
            , cache: false
            , type:'POST'
            , data: {
                login: login,
                password: password
            }
            , success: function () {
                $('#login-modal').modal('hide');
                alert('success');
            }
            , dataType: 'json'
            , error: function() {
                $('#login-modal').modal('hide');
                location.reload();
    }})}

    function logout() {
        $.ajax({
            url:"/account/logout"
            , cache: false
            , type:'POST'
            , success: function () {
                location.reload();
            }
            , error: function () {
                alert('logout error');
            }})

    }
    function registration(login, password, name, email) {
        $.ajax({
            url:"/account/registration"
            , cache: false
            , type:'POST'
            , data: {
                login: login,
                password: password,
                name: name,
                email: email
            }
            , success: function () {
                alert("reg success");
                $('#registration-modal').modal('hide');
                login($("#reg-input-login").val(), $("#reg-input-password").val())

            }
            , dataType: 'json'
            , error: function() {
                alert("reg error");
                $('#registration-modal').modal('hide');
                login($("#reg-input-login").val(), $("#reg-input-password").val())
            }
        });
    }

    function validateLogin(login) {
        return false;
    }

function setLocale ( lang ) {
    $.post(
        "/setlocale/",
        {
            language: lang
        },
        function () {
            location.reload();
        }
    ).fail( function() {
        alert( "Failed to change locale" );
    });
}