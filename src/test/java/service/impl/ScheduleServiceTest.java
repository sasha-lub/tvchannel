package service.impl;

import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.mediacontent.Movie;
import model.mediacontent.Show;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.MovieService;
import service.ScheduleService;

import javax.inject.Inject;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class ScheduleServiceTest {

    @Test
    public void test_NoArticlesByDefault () {
        assertThat( scheduleService.getAllShows(), empty() );
    }

    @Test
    public void test_addItems() throws DuplicatedNameException {
        final Show movie = new Movie("Blue Fire", "Kirkorov and All are waiting for you");
        movieService.add(movie);
        scheduleService.addItem(new LocalDate(2017,1,1), new LocalTime(0,0,0), new LocalTime(2,20,0), movieService.getAll().get(0).getId());
        scheduleService.addItem(new LocalDate(2017,1,1), new LocalTime(10,0,0), new LocalTime(12,20,0), movieService.getAll().get(0).getId());

        assertEquals(2, scheduleService.getAllShows().size());
        assertEquals("Blue Fire", scheduleService.getAllShows().get(0).getShowName());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_addItem_NoSuchShow() throws DuplicatedNameException {

        scheduleService.addItem(new LocalDate(2017,1,1), new LocalTime(10,0,0), new LocalTime(12,20,0), -1);
    }

    @Test
    public void test_getItemsByDate() throws DuplicatedNameException {

        final LocalDate DATE = new LocalDate(2017,1,1);
        final Show movie = new Movie("Blue Fire", "Kirkorov and All are waiting for you");
        movieService.add(movie);
        scheduleService.addItem(DATE, new LocalTime(0,0,0), new LocalTime(2,20,0), movieService.getAll().get(0).getId());
        scheduleService.addItem(DATE, new LocalTime(10,0,0), new LocalTime(12,20,0), movieService.getAll().get(0).getId());

        assertEquals("Blue Fire", scheduleService.getShowsByDate(DATE).get(0).getShowName());
    }

    @Test
    public void test_getItemsByWrongDate() throws DuplicatedNameException {

        final LocalDate DATE = new LocalDate(2017,1,1);
        final LocalDate WRONG_DATE = new LocalDate(2020,11,11);

        final Show movie = new Movie("Blue Fire", "Kirkorov and All are waiting for you");
        movieService.add(movie);
        scheduleService.addItem(DATE, new LocalTime(0,0,0), new LocalTime(2,20,0), movieService.getAll().get(0).getId());
        scheduleService.addItem(DATE, new LocalTime(10,0,0), new LocalTime(12,20,0), movieService.getAll().get(0).getId());

        assertEquals(0, scheduleService.getShowsByDate(WRONG_DATE).size());
    }

    @Test
    public void test_toString() throws DuplicatedNameException {
        final Show movie = new Movie("Blue Fire", "Kirkorov and All are waiting for you");
        movieService.add(movie);
        scheduleService.addItem(new LocalDate(2017,1,1), new LocalTime(0,0,0), new LocalTime(2,20,0), movieService.getAll().get(0).getId());
        scheduleService.addItem(new LocalDate(2017,1,1), new LocalTime(10,0,0), new LocalTime(12,20,0), movieService.getAll().get(0).getId());

        String schedule = scheduleService.toString();
    }

    @Inject
    private MovieService movieService;
    @Inject
    private ScheduleService scheduleService;
}
