package service.impl;

import dto.ShowDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.mediacontent.Movie;
import model.mediacontent.Show;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.MovieService;

import javax.inject.Inject;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class MovieServiceTest {

    @Test
    public void test_NoArticlesByDefault () {
        assertThat( movieService.getAll(), empty() );
    }

    @Test
    public void test_AddMovie () throws DuplicatedNameException {
        final String NAME = "Tvari";
        final String DESCR = "I gde oni obitaut?";
        final Show movie = new Movie(NAME, DESCR);
        movieService.add(movie);

        assertEquals(NAME, movieService.getAll().get(0).getName());
        assertEquals(DESCR, movieService.getAll().get(0).getDescription());
    }

    @Test
    public void test_Add2Movies () throws DuplicatedNameException {
        final String NAME = "Tvari";
        final String DESCR = "I gde oni obitaut?";

        final String NAME2 = "Sausage Party ";
        final String DESCR2 = "ne to cho vy podumali";

        final Show movie = new Movie(NAME, DESCR);
        final Show movie2 = new Movie(NAME2, DESCR2);

        movieService.add(movie);
        movieService.add(movie2);

        assertEquals(NAME, movieService.getByName(NAME).getName());
        assertEquals(DESCR, movieService.getByName(NAME).getDescription());

        assertEquals(NAME2, movieService.getByName(NAME2).getName());
        assertEquals(DESCR2, movieService.getByName(NAME2).getDescription());

        assertEquals(2, movieService.getAll().size());
    }

    @Test(expected = DuplicatedNameException.class)
    public void test_Add_DuplicatedNames () throws DuplicatedNameException {
        final String NAME = "Tvari";
        final String DESCR = "I gde oni obitaut?";

        final String DESCR2 = "ne to cho vy podumali";

        final Show movie = new Movie(NAME, DESCR);
        final Show movie2 = new Movie(NAME, DESCR2);

        movieService.add(movie);
        movieService.add(movie2);
    }

    @Test
    public void test_GetById() throws DuplicatedNameException {

        final String NAME = "Sausage Party ";
        final String DESCR = "ne to cho vy podumali";
        final Show movie = new Movie(NAME, DESCR);

        movieService.add(movie);
        ShowDto dtoMovie = movieService.getByName(NAME);
        ShowDto dtoCompare = movieService.getById(dtoMovie.getId());

        assertEquals(dtoMovie.getName(), dtoCompare.getName());
        assertEquals(dtoMovie.getDescription(), dtoCompare.getDescription());
        assertEquals(dtoMovie.getImageUrl(), dtoCompare.getImageUrl());
        assertEquals(dtoMovie.getId(), dtoCompare.getId());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_GetById_NoSuchEntity() throws DuplicatedNameException {
        movieService.getById(-1);
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_GetByName_NoSuchEntity() throws DuplicatedNameException {
        movieService.getByName("some name");
    }

    @Test
    public void test_ChangeDescr() throws DuplicatedNameException {
        final String NAME = "Sausage Party ";
        final String DESCR = "ne to cho vy podumali";
        final String NEW_DESCR = "but who knows, actually ;)";
        final Show movie = new Movie(NAME, DESCR);

        movieService.add(movie);
        ShowDto dtoMovie = movieService.getByName(NAME);
        movieService.changeDescription(dtoMovie.getId(), NEW_DESCR);
        ShowDto newMovie = movieService.getByName(NAME);

        assertEquals(NEW_DESCR, newMovie.getDescription());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeDescr_NoSuchEntity() {
        movieService.changeDescription(-1, "some txt");
    }

    @Test
    public void test_ChangeImage() throws DuplicatedNameException {
        final String NAME = "Sausage Party ";
        final String DESCR = "ne to cho vy podumali";
        final String NEW_IMG = "picture.jpg";

        movieService.add(new Movie(NAME, DESCR));
        ShowDto movie = movieService.getByName(NAME);

        movieService.changeImage(movie.getId(), NEW_IMG);
        ShowDto newMovie = movieService.getByName(NAME);

        assertEquals(NEW_IMG, newMovie.getImageUrl());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeImg_NoSuchEntity(){
        movieService.changeImage(-1, "someth.png");
    }

    @Test
    public void test_Remove() throws DuplicatedNameException {
        final String NAME = "Sausage Party ";
        final String DESCR = "ne to cho vy podumali";

        movieService.add(new Movie(NAME, DESCR));

        assertEquals(1, movieService.getAll().size());

        ShowDto movie = movieService.getByName(NAME);
        movieService.remove(movie.getId());

        assertEquals(0, movieService.getAll().size());
    }



    @Inject
    private MovieService movieService;
}
