package service.impl;

import dto.AuthorShowDto;
import dto.PresenterDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.mediacontent.AuthorShow;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.AuthorShowService;
import service.PresenterService;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sasha on 28.12.2016.
 */


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class AuthorShowServiceTest {
    @Test
    public void test_AddPresenter() throws DuplicatedNameException {
        final String PRESENTER_NAME = "Leo";

        authorShowService.add(new AuthorShow("Stervochki", "cant wait 5th season!"));
        presenterService.add(PRESENTER_NAME, new LocalDate(1996, 10, 5), "apolon");

        AuthorShowDto authorShow = (AuthorShowDto) authorShowService.getByName("Stervochki");
        PresenterDto presenterDto = presenterService.getAll().get(0);

        authorShowService.addPresenter(authorShow.getId(), presenterDto.getId() );

        AuthorShowDto newAuthorShow = (AuthorShowDto) authorShowService.getByName("Stervochki");

        assertEquals(1, newAuthorShow.getPresenters().size());
        assertEquals(PRESENTER_NAME, newAuthorShow.getPresenters().iterator().next().getName());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_AddPresenter_NoSuchShow(){
        final String PRESENTER_NAME = "Leo";

        presenterService.add(PRESENTER_NAME, new LocalDate(1996, 10, 5), "apolon");

        PresenterDto presenterDto = presenterService.getAll().get(0);

        authorShowService.addPresenter(-1, presenterDto.getId() );
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_AddPresenter_NoSuchPresenter() throws DuplicatedNameException {
        authorShowService.add(new AuthorShow("Stervochki", "cant wait 5th season!"));

        AuthorShowDto authorShow = (AuthorShowDto) authorShowService.getByName("Stervochki");

        authorShowService.addPresenter(authorShow.getId(), -1);
    }

    @Inject
    private PresenterService presenterService;

    @Inject
    private AuthorShowService authorShowService;
}
