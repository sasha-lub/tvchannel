package service.impl;

import dto.ArticleDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.ArticleService;

import javax.inject.Inject;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class ArticleServiceTest {

    @Test
    public void test_NoArticlesByDefault () {
        assertThat( articleService.getAll(), empty() );
    }

    @Test
    public void test_CreateArticle() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String TEXT = "balash alkash";

        articleService.add(HEADER, TEXT);
        ArticleDto article = articleService.getByHeader(HEADER);

        assertEquals(HEADER, article.getHeader());
        assertEquals(TEXT, article.getText());
        assertEquals("/resources/images/article.jpg", article.getImageUrl());
    }

    @Test(expected = DuplicatedNameException.class)
    public void test_CreateArticle_DuplicatedName() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String TEXT = "balash alkash";

        articleService.add(HEADER, TEXT);
        articleService.add(HEADER, TEXT);
    }

    @Test
    public void test_Create2Articles() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String TEXT = "balash alkash";

        final String HEADER2 = "news";
        final String TEXT2 = "first news are not as hot";

        articleService.add(HEADER, TEXT);
        articleService.add(HEADER2, TEXT2);

        ArticleDto article = articleService.getByHeader(HEADER);
        ArticleDto article2 = articleService.getByHeader(HEADER2);

        assertEquals(HEADER, article.getHeader());
        assertEquals(TEXT, article.getText());
        assertEquals("/resources/images/article.jpg", article.getImageUrl());

        assertEquals(HEADER2, article2.getHeader());
        assertEquals(TEXT2, article2.getText());
        assertEquals("/resources/images/article.jpg", article2.getImageUrl());

        assertEquals(2, articleService.getAll().size());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_GetById_NoSuchEntity(){
        articleService.getById(787878781);
    }

    @Test
    public void test_GetByHeader_NoSuchEntity(){
        ArticleDto a = articleService.getByHeader("no such article");
        assertEquals(null, a);
    }

    @Test
    public void test_Remove() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String TEXT = "balash alkash";

        articleService.add(HEADER, TEXT);
        ArticleDto article = articleService.getByHeader(HEADER);
        assertEquals(1, articleService.getAll().size());

        articleService.remove(article.getId());

        assertEquals(0, articleService.getAll().size());
    }

    @Test
    public void test_ChangeHeader() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String NEW_HEADER = "not hot news";
        final String TEXT = "balash alkash";

        articleService.add(HEADER, TEXT);
        ArticleDto article = articleService.getByHeader(HEADER);

        articleService.changeHeader(article.getId(), NEW_HEADER);
        ArticleDto articleNew = articleService.getById(article.getId());
        assertEquals(NEW_HEADER, articleNew.getHeader());
    }

    @Test(expected = DuplicatedNameException.class)
    public void test_ChangeHeader_DuplicatedHeader() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String OTHER_HEADER = "braking news";
        final String TEXT = "balash alkash";

        articleService.add(HEADER, TEXT);
        articleService.add(OTHER_HEADER, TEXT);

        ArticleDto article = articleService.getByHeader(HEADER);

        articleService.changeHeader(article.getId(), OTHER_HEADER);
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeHeader_NoSuchEntity() throws DuplicatedNameException {
        articleService.changeHeader(-1,"new article");
    }

    @Test
    public void test_ChangeText() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String TEXT = "balash alkash";
        final String NEW_TEXT = "vse i tak znali";

        articleService.add(HEADER, TEXT);
        ArticleDto article = articleService.getByHeader(HEADER);

        articleService.changeText(article.getId(), NEW_TEXT);
        ArticleDto articleNew = articleService.getById(article.getId());
        assertEquals(NEW_TEXT, articleNew.getText());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeText_NoSuchEntity(){
        articleService.changeText(-1,"new article");
    }

    @Test
    public void test_ChangeImage() throws DuplicatedNameException {
        final String HEADER = "hot news";
        final String TEXT = "balash alkash";

        articleService.add(HEADER, TEXT);
        ArticleDto article = articleService.getByHeader(HEADER);

        articleService.changeImage(article.getId(), "revo.jpg");
        ArticleDto articleNew = articleService.getById(article.getId());
        assertEquals("revo.jpg", articleNew.getImageUrl());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeImage_NoSuchEntity(){
        articleService.changeImage(-1,"new_article.jpeg");
    }

    @Inject
    private ArticleService articleService;
}
