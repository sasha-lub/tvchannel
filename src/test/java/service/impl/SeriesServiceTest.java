package service.impl;

import dto.SeriesDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.mediacontent.Series;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.SeriesService;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

/**
 * Created by Sasha on 28.12.2016.
 */


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class SeriesServiceTest {
    @Test
    public void test_AddEpisode() throws DuplicatedNameException {
        final String EPISODE_NAME = "Leo vernulsya!";

        seriesService.add(new Series("Stervochki", "cant wait 5th season!"));
        SeriesDto series = (SeriesDto) seriesService.getByName("Stervochki");
        seriesService.addEpisode(series.getId(), EPISODE_NAME, "prikinte!", 4, 10);
        SeriesDto newSeries = (SeriesDto) seriesService.getByName("Stervochki");

        assertEquals(1, newSeries.getEpisodes().size());
        assertEquals(EPISODE_NAME, newSeries.getEpisodes().iterator().next().getName());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_AddEpisode_NoSuchShow(){
        final String EPISODE_NAME = "Leo vernulsya!";

        seriesService.addEpisode(-1, EPISODE_NAME, "prikinte!", 4, 10);
    }

    @Inject
    private SeriesService seriesService;
}
