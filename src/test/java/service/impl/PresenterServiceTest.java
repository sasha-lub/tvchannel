package service.impl;

import dto.PresenterDto;
import exception.NoSuchEntityException;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.PresenterService;

import javax.inject.Inject;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class PresenterServiceTest {

    @Test
    public void test_NoArticlesByDefault () {
        assertThat( presenterService.getAll(), empty() );
    }

    @Test
    public void test_CreatePresenter() {
        final String NAME = "Doljanskiy";
        final LocalDate DOB = new LocalDate(1975, 11, 11);
        final String  DESCRIPTION = "raper";

        presenterService.add(NAME, DOB, DESCRIPTION);
        PresenterDto presenter = presenterService.getAll().get(0);

       assertEquals(NAME, presenter.getName());
       assertEquals(DESCRIPTION, presenter.getDescription());
       assertEquals(DOB, presenter.getDob());
       assertEquals("/resources/images/ava.jpg", presenter.getImageUrl());
    }

    @Test
    public void test_Create2Articles() {
        final String NAME = "Doljanskiy";
        final LocalDate DOB = new LocalDate(1975, 11, 11);
        final String  DESCRIPTION = "raper";

        presenterService.add(NAME, DOB, DESCRIPTION);
        PresenterDto presenter = presenterService.getAll().get(0);

        assertEquals(NAME, presenter.getName());
        assertEquals(DESCRIPTION, presenter.getDescription());
        assertEquals(DOB, presenter.getDob());
        assertEquals("/resources/images/ava.jpg", presenter.getImageUrl());

        final String NAME2 = "Guf";
        final LocalDate DOB2 = new LocalDate(2000, 5, 17);
        final String  DESCRIPTION2 = "poc";

        presenterService.add(NAME2, DOB2, DESCRIPTION2);
        PresenterDto presenter2 = presenterService.getAll().get(1);

        assertEquals(NAME2, presenter2.getName());
        assertEquals(DESCRIPTION2, presenter2.getDescription());
        assertEquals(DOB2, presenter2.getDob());
        assertEquals("/resources/images/ava.jpg", presenter2.getImageUrl());

        assertEquals(2, presenterService.getAll().size());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_GetById_NoSuchEntity(){
        presenterService.getById(-1);
    }

    @Test
    public void test_RemovePresenter() {
        final String NAME = "Doljanskiy";
        final LocalDate DOB = new LocalDate(1975, 11, 11);
        final String  DESCRIPTION = "raper";

        presenterService.add(NAME, DOB, DESCRIPTION);
        assertEquals(1, presenterService.getAll().size());

        PresenterDto presenter = presenterService.getAll().get(0);
        presenterService.remove(presenter.getId());

        assertEquals(0, presenterService.getAll().size());
    }

    @Test
    public void test_ChangeName() {
        final String NAME = "Doljanskiy";
        final String NEW_NAME = "RAP GOD Doljanskiy";
        final LocalDate DOB = new LocalDate(1975, 11, 11);
        final String  DESCRIPTION = "raper";

        presenterService.add(NAME, DOB, DESCRIPTION);

        PresenterDto presenter = presenterService.getAll().get(0);
        presenterService.changeName(presenter.getId(), NEW_NAME);
        PresenterDto presenterNew = presenterService.getById(presenter.getId());

        assertEquals(NEW_NAME, presenterNew.getName());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeName_NoSuchEntity(){
        presenterService.changeName(-1, "alias");
    }

    @Test
    public void test_ChangeDescription() {
        final String NAME = "Doljanskiy";
        final LocalDate DOB = new LocalDate(1975, 11, 11);
        final String  DESCRIPTION = "raper";
        final String  NEW_DESCRIPTION = "top hot raper";

        presenterService.add(NAME, DOB, DESCRIPTION);

        PresenterDto presenter = presenterService.getAll().get(0);
        presenterService.changeDescription(presenter.getId(), NEW_DESCRIPTION);
        PresenterDto presenterNew = presenterService.getAll().get(0);

        assertEquals(NEW_DESCRIPTION, presenterNew.getDescription());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeDescription_NoSuchEntity(){
        presenterService.changeDescription(-1, "something");
    }

    @Test
    public void test_ChangeImage() {
        final String NAME = "Doljanskiy";
        final LocalDate DOB = new LocalDate(1975, 11, 11);
        final String  DESCRIPTION = "raper";
        final String  IMAGE_URL = "path/naked.jpeg";

        presenterService.add(NAME, DOB, DESCRIPTION);

        PresenterDto presenter = presenterService.getAll().get(0);
        presenterService.changeImage(presenter.getId(), IMAGE_URL);
        PresenterDto presenterNew = presenterService.getAll().get(0);

        assertEquals(IMAGE_URL, presenterNew.getImageUrl());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeImage_NoSuchEntity(){
        presenterService.changeImage(-1, "someimg.png");
    }

    @Inject
    private PresenterService presenterService;
}
