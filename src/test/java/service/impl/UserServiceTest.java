package service.impl;

import dto.UserDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import exception.WrongLoginException;
import exception.WrongPasswordException;
import model.account.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.UserService;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by Саня on 26.12.2016.
 */

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = configuration.JUnitConfiguration.class )
@Transactional
public class UserServiceTest {

    @Test
    public void test_NoUsersByDefault () {
        assertThat( userService.getAll(), empty() );
    }

    @Test
    public void test_CreateUser()throws Exception{
        userService.add("bAlAsH", "bal_ebal", "Sasha", "balash@nure.ua");
        UserDto user = userService.getByLogin("bAlAsH");

        assertEquals("Sasha", user.getName());
        assertEquals("balash@nure.ua", user.getEmail());
        assertEquals("/resources/images/ava.jpg", user.getImageUrl());
    }

    @Test
    public void test_Create2Users()throws Exception{
        userService.add("bAlAsH", "bal_ebal", "Sasha", "balash@nure.ua");
        UserDto user1 = userService.getByLogin("bAlAsH");

        assertEquals("Sasha", user1.getName());
        assertEquals("balash@nure.ua", user1.getEmail());
        assertEquals("/resources/images/ava.jpg", user1.getImageUrl());

        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        UserDto user2 = userService.getByLogin("HaHaN");

        assertEquals("Nastya", user2.getName());
        assertEquals("hahan@nure.ua", user2.getEmail());
        assertEquals("/resources/images/ava.jpg", user2.getImageUrl());

        List<UserDto> users = userService.getAll();
        assertEquals(users.size(), 2);

    }

    @Test
    public void test_GetByLoginNoSuchUserEx(){
        UserDto u = userService.getByLogin("no_such_user");
        assertEquals(null, u);
    }

    @Test
    public void test_GetByEmailNoSuchUserEx(){
        UserDto u = userService.getByEmail("no_such_user@nure.ua");
        assertEquals(null, u);
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_GetByIdNoSuchUserEx(){
        userService.getById(-1);
    }

    @Test
    public void test_ChangeImage() throws DuplicatedNameException {
        final String TEST_IMAGE = "path/coolava.jpg";
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changeImage(user.getId(), TEST_IMAGE);
        UserDto userNew = userService.getByLogin("HaHaN");
        assertEquals(TEST_IMAGE, userNew.getImageUrl());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeImageNoSuchEntity() {
        userService.changeImage(-1, "krasotka.png");
    }

    @Test
    public void test_ChangeEmail() throws DuplicatedNameException {
        final String TEST_EMAIL = "hahan@gmail.com";
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changeEmail(user.getId(), TEST_EMAIL);
        UserDto userNew = userService.getById(user.getId());
        assertEquals(TEST_EMAIL, userNew.getEmail());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeEmailNoSuchEntity() throws DuplicatedNameException {
        userService.changeEmail(-1, "krasotka@mail.ru");
    }

    @Test(expected = DuplicatedNameException.class)
    public void test_ChangeEmail_DuplicatedNames() throws DuplicatedNameException {
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        userService.add("bAlAsH", "bal_ebal", "Sasha", "balash@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changeEmail(user.getId(), "balash@nure.ua");
    }

    @Test
    public void test_ChangeName() throws DuplicatedNameException {
        final String TEST_NAME = "Anastasia";
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changeName(user.getId(), TEST_NAME);
        UserDto userNew = userService.getByLogin("HaHaN");
        assertEquals(TEST_NAME, userNew.getName());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeNameNoSuchEntity() {
        userService.changeName(-1, "krasotka");
    }

    @Test
    public void test_ChangeRole() throws DuplicatedNameException {
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.setRole(user.getId(), Role.ADMIN);
        UserDto userNew = userService.getByLogin("HaHaN");
        assertEquals(Role.ADMIN, userNew.getRole());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeRoleNoSuchEntity() {
        userService.setRole(-1, Role.ADMIN);
    }

    @Test
    public void test_ChangeLogin() throws DuplicatedNameException {
        final String TEST_LOGIN = "vodka";
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changeLogin(user.getId(), TEST_LOGIN);
        UserDto userNew = userService.getById(user.getId());
        assertEquals(TEST_LOGIN, userNew.getLogin());
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangeLoginNoSuchEntity() throws DuplicatedNameException {
        userService.changeLogin(-1, "krasotka@mail.ru");
    }

    @Test(expected = DuplicatedNameException.class)
    public void test_ChangeLogin_DuplicatedNames() throws DuplicatedNameException {
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");
        userService.add("bAlAsH", "bal_ebal", "Sasha", "balash@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changeLogin(user.getId(), "bAlAsH");
    }

    @Test
    public void test_ChangePassword() throws DuplicatedNameException, WrongPasswordException {
        final String OLD_PASS = "hahan_pahan";
        final String NEW_PASS = "pahan_hahan";
        userService.add("HaHaN", OLD_PASS, "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changePassword(user.getId(), OLD_PASS, NEW_PASS);
    }

    @Test(expected = WrongPasswordException.class)
    public void test_ChangePassword_WrongOldPassword() throws DuplicatedNameException, WrongPasswordException {
        final String OLD_PASS = "hahan_pahan";
        final String WRONG_PASS = "hahan_stakan";
        final String NEW_PASS = "pahan_hahan";
        userService.add("HaHaN", OLD_PASS, "Nastya", "hahan@nure.ua");
        UserDto user = userService.getByLogin("HaHaN");
        userService.changePassword(user.getId(), WRONG_PASS, NEW_PASS);
    }

    @Test(expected = NoSuchEntityException.class)
    public void test_ChangePassword_NoSuchEntity() throws WrongPasswordException {
        userService.changePassword(-1, "oldPass", "newPass");
    }

    @Test
    public void test_GetByEmail()throws Exception{
        userService.add("bAlAsH", "bal_ebal", "Sasha", "balash@nure.ua");
        UserDto user = userService.getByEmail("balash@nure.ua");
        assertEquals("Sasha", user.getName());
        assertEquals("balash@nure.ua", user.getEmail());
        assertEquals("/resources/images/ava.jpg", user.getImageUrl());
    }

    @Test
    public void test_GetById()throws Exception{
        userService.add("bAlAsH", "bal_ebal", "Sasha", "balash@nure.ua");
        UserDto user = userService.getByEmail("balash@nure.ua");
        UserDto user2 = userService.getById(user.getId());

        assertEquals(user.getEmail(), user2.getEmail());
        assertEquals(user.getName(), user2.getName());
        assertEquals(user.getId(), user2.getId());
    }

    @Test( expected = DuplicatedNameException.class )
    public void test_CreateDuplicateEmailUser () throws Exception {
        userService.add( "BALASH", "bAlAsH321", "Sasha", "student@nure.ua" );
        userService.add( "SEREDA", "SrEdA12345" , "Vlados","student@nure.ua" );
    }

    @Test( expected = DuplicatedNameException.class )
    public void test_CreateDuplicateLoginUser () throws Exception {
        userService.add( "STUDENT", "bAlAsH321", "Sasha", "blsh@nure.ua" );
        userService.add( "STUDENT", "SerEdA12345" , "Vlados","srd@nure.ua" );
    }

    @Test
    public void test_Remove() throws DuplicatedNameException {
        userService.add("HaHaN", "hahan_pahan", "Nastya", "hahan@nure.ua");

        assertEquals(1, userService.getAll().size());

        UserDto user = userService.getByLogin("HaHaN");
        userService.remove(user.getId());

        assertEquals(0, userService.getAll().size());
    }

    @Test
    public void test_Login() throws DuplicatedNameException, WrongPasswordException, WrongLoginException {
        final String PASS = "hahan_pahan";
        final String LOGIN = "HaHaN";

        userService.add(LOGIN, PASS, "Nastya", "hahan@nure.ua");

        userService.login(LOGIN, PASS);
    }

    @Test(expected = WrongLoginException.class)
    public void test_Login_WrongLogin() throws DuplicatedNameException, WrongPasswordException, WrongLoginException {
        final String PASS = "hahan_pahan";
        final String LOGIN = "HaHaN";
        final String WRONG_LOGIN = "hahan";

        userService.add(LOGIN, PASS, "Nastya", "hahan@nure.ua");

        userService.login(WRONG_LOGIN, PASS);
    }

    @Test(expected = WrongPasswordException.class)
    public void test_Login_WrongPassword() throws DuplicatedNameException, WrongPasswordException, WrongLoginException {
        final String PASS = "hahan_pahan";
        final String LOGIN = "HaHaN";
        final String WRONG_PASS = "hahan";

        userService.add(LOGIN, PASS, "Nastya", "hahan@nure.ua");

        userService.login(LOGIN, WRONG_PASS);
    }


    @Inject
    private UserService userService;
}
