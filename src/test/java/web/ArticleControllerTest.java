package web;

import dto.ArticleDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import service.ArticleService;
import web.constants.AttributeNames;
import web.constants.ViewNames;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( MockitoJUnitRunner.class )
public class ArticleControllerTest {

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
        article = new ArticleDto(1, "New President USA", "Nobody is happy :( ", "pict.jpg");

    }

    @Test
    public void test_ViewArticle() throws Exception {
        when(mockArticleServise.getById(1)).thenReturn(article);

        this.mockMvc.perform(
                get("/article/")
                        .param("articleId", "1")
        ).andExpect(model().attribute( AttributeNames.ArticleView.Article, article))
        .andExpect(status().isOk())
        .andExpect(view().name(ViewNames.Article))
    ;
    }



    @Mock
    private ArticleService mockArticleServise;

    @InjectMocks
    private ArticleController theController;

    private MockMvc mockMvc;

    private ArticleDto article;
}
