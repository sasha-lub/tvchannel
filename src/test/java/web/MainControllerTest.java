package web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import service.ArticleService;
import service.MovieService;
import service.PresenterService;
import service.ScheduleService;
import web.constants.ViewNames;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( MockitoJUnitRunner.class )
public class MainControllerTest {

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }

    @Test
    public void test_MainInitializer() throws Exception {
        this.mockMvc.perform(
                get("/main/")
        ).andExpect(status().isOk())
        .andExpect(view().name(ViewNames.Main));

        verify(mockMovieServise).getAll() ;
        verify(mockArticleService).getAll() ;
        verify(mockPresenterService).getAll() ;
        verify(mockScheduleService).getAllShows() ;

    }



    @Mock
    private MovieService mockMovieServise;

    @Mock
    private ArticleService mockArticleService;

    @Mock
    private PresenterService mockPresenterService;

    @Mock
    private ScheduleService mockScheduleService;

    @InjectMocks
    private MainController theController;

    private MockMvc mockMvc;
}
