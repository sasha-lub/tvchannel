package web;

import exception.DuplicatedNameException;
import exception.WrongLoginException;
import exception.WrongPasswordException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import service.UserService;
import dto.UserDto;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( MockitoJUnitRunner.class )
public class AccountControllerTest {
    @Before
    public void setup () throws DuplicatedNameException {
        login = "sasha_lub";
        password = "myPass";
        name="Sasha";
        email="sasha@nure.ua";
        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
    }

    @Test
    public void test_ViewAccount_Get () throws Exception {
        this.mockMvc.perform(
                get("/account/myAccount")
        ).andExpect(status().isOk());
    }

    @Test
    public void test_Login_Post () throws Exception
    {
        this.mockMvc.perform(
                post( "/account/login" )
                .param("login", login)
                .param("password", password)
        ).andExpect(status().isOk())
                .andExpect(request().sessionAttribute("user", mockUserService.getByLogin(login)))
        ;

        verify(mockUserService).login(login, password) ;
    }

    @Test
    public void test_Login_Post_WrongLogin () throws Exception
    {
        when(mockUserService.login(login, password)).thenThrow(WrongLoginException.class);
        this.mockMvc.perform(
                post( "/account/login" )
                        .param("login", login)
                        .param("password", password)
        ).andExpect(status().isNotAcceptable());
        ;

        verify(mockUserService).login(login, password) ;
    }

    @Test
    public void test_Login_Post_WrongPass () throws Exception
    {
        when(mockUserService.login(login, password)).thenThrow(WrongPasswordException.class);
        this.mockMvc.perform(
                post( "/account/login" )
                        .param("login", login)
                        .param("password", password)
        ).andExpect(status().isNotAcceptable());
        ;

        verify(mockUserService).login(login, password) ;
    }

    @Test
    public void test_Reg_Post () throws Exception
    {
        this.mockMvc.perform(
                post( "/account/registration" )
                .param("login", login)
                .param("password", password)
                .param("name", name)
                .param("email", email)
        ).andExpect(status().isCreated())
        ;
       verify( mockUserService).add(login, password, name, email);;
    }

    @Test
    public void test_Reg_Post_DuplicatedLogin () throws Exception
    {
        doThrow(DuplicatedNameException.class).when(mockUserService).add(login, password, name, email);
        this.mockMvc.perform(
                post( "/account/registration" )
                        .param("login", login)
                        .param("password", password)
                        .param("name", name)
                        .param("email", email)
        ).andExpect(status().isConflict())
        ;
        verify( mockUserService).add(login, password, name, email);;
    }

    @Test
    public void test_Logout() throws Exception {
        this.mockMvc.perform(
                get("/account/logout")
        ).andExpect(status().isOk());
    }

    @Mock
    private UserService mockUserService;

    private String login, password, name, email;

    @InjectMocks
    private AccountController theController;

    private MockMvc mockMvc;
}
