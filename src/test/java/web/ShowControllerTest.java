package web;

import dto.MovieDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import service.MovieService;
import web.constants.AttributeNames;
import web.constants.ViewNames;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Sasha on 27.12.2016.
 */

@RunWith( MockitoJUnitRunner.class )
public class ShowControllerTest {

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup( theController ).build();
        movie = new MovieDto(1, "Borat", "black humor", "", null, "pict.jpg");

    }

    @Test
    public void test_ViewShow() throws Exception {
        when(mockMovieService.getById(1)).thenReturn(movie);

        this.mockMvc.perform(
                get("/show/")
                        .param("showId", "1")
        ).andExpect(model().attribute( AttributeNames.ShowView.Show, movie))
        .andExpect(status().isOk())
        .andExpect(view().name(ViewNames.Show))

        ;
    }

    @Mock
    private MovieService mockMovieService;

    @InjectMocks
    private ShowController theController;

    private MockMvc mockMvc;

    private MovieDto movie;
}
