package model;

import model.account.Role;
import model.account.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Саня on 25.12.2016.
 */
public class UserTest {

    @Test
    public void test_DefaultConstructor(){
        User u = new User();

        assertEquals(u.getId(), null);
        assertEquals(u.getName(), null);
        assertEquals(u.getEmail(), null);
        assertEquals(u.getLogin(), null);
        assertEquals(u.getPassword(), null);
        assertEquals(u.getRole(), null);
        assertEquals(u.getImageUrl(), "/resources/images/ava.jpg");
    }

    @Test
    public void test_PublicConstructor(){
        final String TEST_NAME = "Sasha";
        final String TEST_LOGIN = "sasha_lub";
        final String TEST_EMAIL = "lubchenko@nure.ua";
        final String TEST_PASSWORD = "sasha123";

        User u = new User(TEST_LOGIN, TEST_PASSWORD, TEST_NAME, TEST_EMAIL);

        assertEquals(u.getId(), null);
        assertEquals(u.getName(), TEST_NAME);
        assertEquals(u.getEmail(), TEST_EMAIL);
        assertEquals(u.getLogin(), TEST_LOGIN);
        assertEquals(u.getPassword(), TEST_PASSWORD);
        assertEquals(u.getRole(), Role.USER);
        assertEquals(u.getImageUrl(), "/resources/images/ava.jpg");
    }

    @Test
    public void test_Equals(){
        User u1 = new User("balash0v", "balash123", "balash", "balash@nure.ua");
        User u2 = new User("balash0v", "balash123", "balash", "balash@nure.ua");

        assertEquals(u1, u2);
    }

    @Test
    public void test_Equals_DiffLogin(){
        User u1 = new User("balash0v", "balash123", "balash", "balash@nure.ua");
        User u2 = new User("balashOv", "balash123", "balash", "balash@nure.ua");

        assertNotEquals(u1, u2);
    }

    @Test
    public void test_Equals_DiffEmail(){
        User u1 = new User("balash0v", "balash123", "balash", "balash@nure.ua");
        User u2 = new User("balash0v", "balash123", "balash", "balash@mail.ru");

        assertNotEquals(u1, u2);
    }

    @Test
    public void test_Equals_DiffName(){
        User u1 = new User("balash0v", "balash123", "balash", "balash@nure.ua");
        User u2 = new User("balash0v", "balash123", "sasha", "balash@nure.ua");

        assertEquals(u1, u2);
    }

    @Test
    public void test_SetName(){
        User u = new User();
        final String TEST_NAME = "Oleksandra";
        u.setName(TEST_NAME);
        assertEquals(u.getName(), TEST_NAME);
    }

    @Test
    public void test_SetLogin(){
        User u = new User();
        final String TEST_LOGIN = "Oleksandra";
        u.setLogin(TEST_LOGIN);
        assertEquals(u.getLogin(), TEST_LOGIN);
    }

    @Test
    public void test_SetPassword(){
        User u = new User();
        final String TEST_PASSWORD = "Oleksandra123";
        u.setPassword(TEST_PASSWORD);
        assertEquals(u.getPassword(), TEST_PASSWORD);
    }

    @Test
    public void test_SetEmail(){
        User u = new User();
        final String TEST_EMAIL = "Oleksandra@nyre.ua";
        u.setEmail(TEST_EMAIL);
        assertEquals(u.getEmail(), TEST_EMAIL);
    }

    @Test
    public void test_SetRole(){
        User u = new User();
        final Role TEST_ROLE = Role.ADMIN;
        u.setRole(TEST_ROLE);
        assertEquals(u.getRole(), TEST_ROLE);
    }

    @Test
    public void test_Set_Img ()
    {
        User u = new User();
        final String TEST_IMG = "somepath/coolpic.jpeg";

        u.setImageUrl(TEST_IMG);

        assertEquals( u.getImageUrl(), TEST_IMG);
    }


}
