package model;

import model.mediacontent.Movie;
import model.mediacontent.Show;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

/**
 * Created by Саня on 25.12.2016.
 */
public class ScheduleItemTest {

    @Test
    public void test_DefaultConstructor ()
    {
        ScheduleItem si = new ScheduleItem();
        assertEquals( si.getShow(), null );
        assertEquals( si.getId(), null );
        assertEquals( si.getDate(), null );
        assertEquals( si.getStartTime(), null);
        assertEquals( si.getEndTime(), null);
    }

    @Test
    public void test_PublicConstructor ()
    {
        final Show TEST_SHOW = new Movie();
        final LocalDate TEST_DATE = new LocalDate(2018, 4, 26);
        final LocalTime TEST_START_TIME = new LocalTime(13, 0, 0);
        final LocalTime TEST_END_TIME = new LocalTime(14, 50, 0);

        ScheduleItem si = new ScheduleItem(TEST_DATE, TEST_START_TIME, TEST_END_TIME, TEST_SHOW);
        assertEquals( si.getShow(), TEST_SHOW );
        assertEquals( si.getDate(), TEST_DATE );
        assertEquals( si.getStartTime(), TEST_START_TIME );
        assertEquals( si.getEndTime(), TEST_END_TIME );
    }

    @Test
    public void test_Set_Show ()
    {
        final Show TEST_SHOW = new Movie();
        ScheduleItem si = new ScheduleItem();
        si.setShow(TEST_SHOW);
        assertEquals( si.getShow(), TEST_SHOW );
    }

    @Test
    public void test_Set_Date ()
    {
        final LocalDate TEST_DATE = new LocalDate(2018, 4, 26);
        ScheduleItem si = new ScheduleItem();
        si.setDate(TEST_DATE);
        assertEquals( si.getDate(), TEST_DATE );
    }

    @Test
    public void test_Set_StartTime ()
    {
        final LocalTime TEST_START_TIME = new LocalTime(13, 0, 0);
        ScheduleItem si = new ScheduleItem();
        si.setStartTime(TEST_START_TIME);
        assertEquals( si.getStartTime(), TEST_START_TIME );
    }

    @Test
    public void test_Set_EndTime ()
    {
        final LocalTime TEST_END_TIME = new LocalTime(13, 0, 0);
        ScheduleItem si = new ScheduleItem();
        si.setEndTime(TEST_END_TIME);
        assertEquals( si.getEndTime(), TEST_END_TIME );
    }
}
