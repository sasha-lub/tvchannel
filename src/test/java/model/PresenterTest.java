package model;

import model.mediacontent.AuthorShow;
import org.joda.time.LocalDate;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Саня on 24.12.2016.
 */
public class PresenterTest {
    @Test
    public void test_DefaultConstructor ()
    {
        Presenter p = new Presenter();
        assertEquals( p.getName(), null );
        assertEquals( p.getDob(), null );
        assertEquals( p.getDescription(), null );
        assertEquals( p.getShows(), new HashSet<AuthorShow>());
        assertEquals( p.getId(), null );
        assertEquals( p.getImageUrl(), "/resources/images/ava.jpg");
    }

    @Test
    public void test_PublicConstructor ()
    {
        final String TEST_NAME = "Karpachev";
        final LocalDate TEST_DOB = new LocalDate(1978, 12, 4);
        final String TEST_DESCRIPTION= "Power boy";

        Presenter p = new Presenter(TEST_NAME, TEST_DOB, TEST_DESCRIPTION);
        assertEquals( p.getName(), TEST_NAME );
        assertEquals( p.getDob(), TEST_DOB );
        assertEquals( p.getDescription(), TEST_DESCRIPTION );
        assertEquals( p.getShows(), new HashSet<AuthorShow>() );
        assertEquals( p.getId(), null );
        assertEquals( p.getImageUrl(), "/resources/images/ava.jpg");
    }

    @Test
    public void test_Equals ()
    {
        Presenter p1 = new Presenter(
                "Sasha Gray",
                new LocalDate(1990, 12, 11),
                "power girl"
        );

        Presenter p2 = new Presenter(
                "Sasha Gray",
                new LocalDate(1990, 12, 11),
                "power girl"
        );

        assertEquals( p1, p2 );
    }


    @Test
    public void test_Equals_Diff_Names ()
    {
        Presenter p1 = new Presenter(
                "Sasha Gray",
                new LocalDate(1990, 12, 11),
                "power girl"
        );

        Presenter p2 = new Presenter(
                "Sasha Green",
                new LocalDate(1990, 12, 11),
                "power girl"
        );

        assertNotEquals( p1, p2 );
    }

    @Test
    public void test_Equals_Diff_Dob ()
    {
        Presenter p1 = new Presenter(
                "Sasha Gray",
                new LocalDate(1980, 12, 11),
                "power girl"
        );

        Presenter p2 = new Presenter(
                "Sasha Gray",
                new LocalDate(1990, 12, 11),
                "power girl"
        );

        assertNotEquals( p1, p2 );
    }

    @Test
    public void test_Set_Img ()
    {
        Presenter p1 = new Presenter();
        final String TEST_IMG = "somepath/coolpic.jpeg";

        p1.setImageUrl(TEST_IMG);

        assertEquals( p1.getImageUrl(), TEST_IMG);
    }

    @Test
    public void test_Set_Name ()
    {
        Presenter p1 = new Presenter();
        final String TEST_NAME = "Sherlock";

        p1.setName(TEST_NAME);

        assertEquals( p1.getName(), TEST_NAME);
    }

    @Test
    public void test_Set_Description ()
    {
        Presenter p1 = new Presenter();
        final String TEST_DESCR = "nice guy";

        p1.setDescription(TEST_DESCR);

        assertEquals( p1.getDescription(), TEST_DESCR);
    }


    @Test
    public void test_Add_Show ()
    {
        Presenter p = new Presenter();
        p.addShow(new AuthorShow());

        assertEquals( p.getShows().size(), 1 );
    }
}
