package model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Саня on 25.12.2016.
 */
public class ArticleTest {
    @Test
    public void test_DefaultConstructor ()
    {
        Article a = new Article();
        assertEquals( a.getHeader(), null );
        assertEquals( a.getId(), null );
        assertEquals( a.getText(), null );
        assertEquals( a.getImageUrl(), "/resources/images/article.jpg");
    }

    @Test
    public void test_PublicConstructor ()
    {
        final String TEST_HEADER = "hot news";
        final String TEST_TEXT = "kolobok povesilsya";

        Article a = new Article(TEST_HEADER, TEST_TEXT);
        assertEquals( a.getHeader(), TEST_HEADER );
        assertEquals( a.getId(), null );
        assertEquals( a.getText(), TEST_TEXT );
        assertEquals( a.getImageUrl(), "/resources/images/article.jpg");
    }

    @Test
    public void test_Equals ()
    {
        Article a1 = new Article(
                "Christen Stuart dating a girl",
                "Christen Stuart was seen with Victoria Secret model"
        );

        Article a2 = new Article(
                "Christen Stuart dating a girl",
                "Christen Stuart was seen with Victoria Secret model"
        );

        assertEquals( a1, a2 );
    }


    @Test
    public void test_Equals_Diff_Header()
    {
        Article a1 = new Article(
                "Christen Stuart dating a girl",
                "Christen Stuart was seen with Victoria Secret model"
        );

        Article a2 = new Article(
                "Stuart Little dating a girl",
                "Christen Stuart was seen with Victoria Secret model"
        );

        assertNotEquals( a1, a2 );
    }

    @Test
    public void test_Equals_Diff_Text ()
    {   Article a1 = new Article(
            "Christen Stuart dating a girl",
            "Christen Stuart was seen with Victoria Secret model"
    );

        Article a2 = new Article(
                "Christen Stuart dating a girl",
                "Christen Stuart was seen with Kostyan Secret model"
        );

        assertNotEquals( a1, a2 );
    }

    @Test
    public void test_Set_Img ()
    {
        Article a = new Article();
        final String TEST_IMG = "somepath/coolpic.jpeg";

        a.setImageUrl(TEST_IMG);

        assertEquals( a.getImageUrl(), TEST_IMG);
    }

    @Test
    public void test_Set_Header ()
    {
        Article a = new Article();
        final String TEST_HEADER = "not as hot news";

        a.setHeader(TEST_HEADER);

        assertEquals( a.getHeader(), TEST_HEADER);
    }

    @Test
    public void test_Set_Text ()
    {
        Article a = new Article();
        final String TEST_TEXT = "nothing happened";

        a.setText(TEST_TEXT);

        assertEquals( a.getText(), TEST_TEXT);
    }
}
