package model;

import model.mediacontent.Genre;
import model.mediacontent.Movie;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Саня on 26.12.2016.
 */
public class MovieTest {
    @Test
    public void test_DefaultConstructor(){
        Movie m = new Movie();

        assertEquals(null, m.getId());
        assertEquals(null, m.getName());
        assertEquals(null, m.getDescription());
        assertEquals(new ArrayList<Genre>(), m.getGenres());
        assertEquals("/resources/images/video.jpg", m.getImageUrl());
    }

    @Test
    public void test_PublicConstructor(){
        final String TEST_NAME = "Borat";
        final String TEST_DESCR = "black humor";

        Movie m = new Movie(TEST_NAME, TEST_DESCR);

        assertEquals(null, m.getId());
        assertEquals(TEST_NAME, m.getName());
        assertEquals(TEST_DESCR, m.getDescription());
        assertEquals(new ArrayList<Genre>(), m.getGenres());
        assertEquals("/resources/images/video.jpg", m.getImageUrl());
    }

    @Test
    public void test_Equals(){
        Movie m1 = new Movie("Borat", "not bad");
        Movie m2 = new Movie("Borat", "not bad");

        assertEquals(m1, m2);
    }

    @Test
    public void test_Equals_DiffNames(){
        Movie m1 = new Movie("Borat", "not bad");
        Movie m2 = new Movie("Ali G", "not bad");

        assertNotEquals(m1, m2);
    }

    @Test
    public void test_SetDesc(){
        final String TEST_DESCR = "lul";

        Movie m = new Movie();
        m.setDescription(TEST_DESCR);
        assertEquals(TEST_DESCR, m.getDescription());
    }

    @Test
    public void test_SetImageUrl(){
        final String TEST_IMAGE = "path/cute_pic.png";

        Movie m = new Movie();
        m.setImageUrl(TEST_IMAGE);
        assertEquals(TEST_IMAGE, m.getImageUrl());
    }

    @Test
    public void test_SetUrl(){
        final String TEST_URL = "blahblah";

        Movie m = new Movie();
        m.setUrl(TEST_URL);
        assertEquals(TEST_URL, m.getUrl());
    }
}
