package model;

import model.mediacontent.Episode;
import model.mediacontent.Genre;
import model.mediacontent.Series;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Саня on 26.12.2016.
 */
public class SeriesTest {
    @Test
    public void test_DefaultConstructor(){
        Series s = new Series();

        assertEquals(null, s.getId());
        assertEquals(null, s.getName());
        assertEquals(null, s.getDescription());
        assertEquals(new ArrayList<Genre>(), s.getGenres());
        assertEquals(new HashSet<Episode>(), s.getEpisodes());
        assertEquals("/resources/images/video.jpg", s.getImageUrl());
    }

    @Test
    public void test_PublicConstructor(){
        final String TEST_NAME = "Fizruk";
        final String TEST_DESCR = "bold guy";

        Series s = new Series(TEST_NAME, TEST_DESCR);

        assertEquals(null, s.getId());
        assertEquals(TEST_NAME, s.getName());
        assertEquals(TEST_DESCR, s.getDescription());
        assertEquals(new ArrayList<Genre>(), s.getGenres());
        assertEquals(new HashSet<Episode>(), s.getEpisodes());
        assertEquals("/resources/images/video.jpg", s.getImageUrl());
    }

    @Test
    public void test_AddEpisode(){
        final Episode TEST_EPISODE = new Episode();
        Series s = new Series();
        assertFalse(s.getEpisodes().contains(TEST_EPISODE));
        s.addEpisode(TEST_EPISODE);
        assertTrue(s.getEpisodes().contains(TEST_EPISODE));
    }

}
