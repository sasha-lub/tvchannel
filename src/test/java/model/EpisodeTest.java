package model;

import model.mediacontent.Episode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Саня on 25.12.2016.
 */
public class EpisodeTest {
    @Test
    public void test_DefaultConstructor(){
        Episode e = new Episode();
        assertEquals(e.getId(), null);
        assertEquals(e.getName(), null);
        assertEquals(e.getEpisodeNumber(), 0);
        assertEquals(e.getSeason(), 0);
        assertEquals(e.getDescription(), null);
        assertEquals(e.getImageUrl(), "/resources/images/video.jpg");
    }

    @Test
    public void test_PublicConstructor(){
        final String TEST_NAME = "reckless action";
        final String TEST_DESCRIPTION = "main character dies, jk";

        Episode e = new Episode(TEST_NAME, TEST_DESCRIPTION);

        assertEquals(e.getId(), null);
        assertEquals(e.getName(), TEST_NAME);
        assertEquals(e.getEpisodeNumber(), 0);
        assertEquals(e.getSeason(), 0);
        assertEquals(e.getDescription(), TEST_DESCRIPTION);
        assertEquals(e.getImageUrl(), "/resources/images/video.jpg");
    }

    @Test
    public void test_PublicExpConstructor(){
        final String TEST_NAME = "reckless action";
        final String TEST_DESCRIPTION = "main character dies, jk";
        final int TEST_SEASON = 3;
        final int TEST_NUM = 3;
        Episode e = new Episode(TEST_NAME, TEST_DESCRIPTION, TEST_SEASON, TEST_NUM);

        assertEquals(e.getId(), null);
        assertEquals(e.getName(), TEST_NAME);
        assertEquals(e.getEpisodeNumber(), TEST_NUM);
        assertEquals(e.getSeason(), TEST_SEASON);
        assertEquals(e.getDescription(), TEST_DESCRIPTION);
        assertEquals(e.getImageUrl(), "/resources/images/video.jpg");
    }

    @Test
    public void test_Equals(){
        Episode e1 = new Episode("episode 1", "cool episode");
        Episode e2 = new Episode("episode 1", "cool episode");

        assertEquals(e1, e2);
    }

    @Test
    public void test_Equals_DiffSeasons(){
        Episode e1 = new Episode("episode 1", "cool episode");
        e1.setSeason(1);
        Episode e2 = new Episode("episode 1", "cool episode");
        e2.setSeason(2);

        assertNotEquals(e1, e2);
    }

    @Test
    public void test_Equals_DiffEpisodeNumbers(){
        Episode e1 = new Episode("episode 1", "cool episode");
        e1.setEpisodeNumber(1);
        Episode e2 = new Episode("episode 1", "cool episode");
        e2.setEpisodeNumber(2);

        assertNotEquals(e1, e2);
    }

    @Test
    public void test_Equals_DiffNames(){
        Episode e1 = new Episode("episode 01", "cool episode");
        Episode e2 = new Episode("episode 1", "cool episode");

        assertNotEquals(e1, e2);
    }

    @Test
    public void test_SetName(){
        final String TEST_NAME = "reckless action";

        Episode e = new Episode();
        e.setName(TEST_NAME);

        assertEquals(TEST_NAME, e.getName());
    }

    @Test
    public void test_SetEpisodeNum(){
        final int TEST_NUM = 4;

        Episode e = new Episode();
        e.setEpisodeNumber(TEST_NUM);

        assertEquals(TEST_NUM, e.getEpisodeNumber());
    }

    @Test
    public void test_SetSeason(){
        final int TEST_NUM = 4;

        Episode e = new Episode();
        e.setSeason(TEST_NUM);

        assertEquals(TEST_NUM, e.getSeason());
    }

    @Test
    public void test_SetImage(){
        final String TEST_IMAGE = "somepath/coolpic.jpg";

        Episode e = new Episode();
        e.setImageUrl(TEST_IMAGE);

        assertEquals(TEST_IMAGE, e.getImageUrl());
    }

    @Test
    public void test_SetDescription(){
        final String TEST_DESCR = "blah-blah-blah";

        Episode e = new Episode();
        e.setDescription(TEST_DESCR);

        assertEquals(TEST_DESCR, e.getDescription());
    }
}
