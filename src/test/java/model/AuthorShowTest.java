package model;

import model.mediacontent.AuthorShow;
import model.mediacontent.Genre;
import model.mediacontent.Episode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Саня on 26.12.2016.
 */
public class AuthorShowTest {
    @Test
    public void test_DefaultConstructor(){
        AuthorShow as = new AuthorShow();

        assertEquals(null, as.getId());
        assertEquals(null, as.getName());
        assertEquals(null, as.getDescription());
        assertEquals(new ArrayList<Genre>(), as.getGenres());
        assertEquals(new HashSet<Episode>(), as.getEpisodes());
        assertEquals(new HashSet<Presenter>(), as.getPresenters());
        assertEquals("/resources/images/video.jpg", as.getImageUrl());
    }

    @Test
    public void test_PublicConstructor(){
        final String TEST_NAME = "SLOVO";
        final String TEST_DESCR = "Poshumim bl*t' !";

        AuthorShow as = new AuthorShow(TEST_NAME, TEST_DESCR);

        assertEquals(null, as.getId());
        assertEquals(TEST_NAME, as.getName());
        assertEquals(TEST_DESCR, as.getDescription());
        assertEquals(new ArrayList<Genre>(), as.getGenres());
        assertEquals(new HashSet<Episode>(), as.getEpisodes());
        assertEquals(new HashSet<Presenter>(), as.getPresenters());
        assertEquals("/resources/images/video.jpg", as.getImageUrl());
    }

    @Test
    public void test_AddPresenter(){
        final Presenter TEST_PRESENTER = new Presenter();
        AuthorShow as = new AuthorShow();
        assertFalse(as.getPresenters().contains(TEST_PRESENTER));
        as.addPresenter(TEST_PRESENTER);
        assertTrue(as.getPresenters().contains(TEST_PRESENTER));
    }
}
