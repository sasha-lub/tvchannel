package model;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "article")
@Access( AccessType.FIELD )
public class Article {

    @Id
    @GeneratedValue
    private Integer id;

    @Pattern(regexp = "[А-ЯA-ZЁЇІЄа-яa-zёїіє0-9'`\"-—«»@!#№$% :&,?]+")
    @NotBlank
    @Column(nullable = false)
    private String header;

    @NotBlank
    @Column(nullable = false, columnDefinition="TEXT")
    private String text;

    @Column
    private String imageUrl = "/resources/images/article.jpg";

    public Article() {
    }

    public Article(String header, String text) {
        this.header = header;
        this.text = text;
    }

//    @Override
//    public String toString() {
//        return "***Article***" + System.lineSeparator()
//                + header + System.lineSeparator()
//                + text + System.lineSeparator() + System.lineSeparator();
//    }

    public Integer getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (null == o) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }

        if (!this.getHeader().trim().equals(((Article) o).getHeader().trim())) {
            return false;
        }else if (!this.getText().equals(((Article) o).getText())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash += hash * 31 + ((this.getHeader() == null) ? 0 : this.getHeader().hashCode());
        hash += hash * 31 + ((this.getText() == null) ? 0 : this.getText().hashCode());

        return hash;
    }
}