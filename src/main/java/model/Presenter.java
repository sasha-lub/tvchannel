package model;

import model.mediacontent.AuthorShow;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Past;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "presenter")
public class Presenter {

    @Id
    @GeneratedValue
    private Integer id;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @Past
    @Column
    private LocalDate dob;

    @Column(columnDefinition="text")
    private String description;

    @Valid
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<AuthorShow> shows = new HashSet<AuthorShow>();

    @Column
    private String imageUrl = "/resources/images/ava.jpg";

    public Presenter(){
    }

    public Presenter(String name, LocalDate dob, String description) {
        this.name = name;
        this.dob = dob;
        this.description = description;
    }

//    @Override
//    public String toString() {
//        StringBuilder sb = new StringBuilder();
//        sb.append("***Presenter***").append(System.lineSeparator())
//                .append("name : ").append(name)
//                .append(", date of birth : ").append(dob.toString("dd.MM.yyyy"))
//                .append(System.lineSeparator()).append(description).append(System.lineSeparator());
//        if (shows != null && !shows.isEmpty()) {
//            for (AuthorShow show : shows) {
//                sb.append(show.getName()).append(System.lineSeparator());
//            }
//        }
//        sb.append(System.lineSeparator());
//        return sb.toString();
//    }

    public void addShow(AuthorShow show) {
        shows.add(show);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDob() {
        return dob;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<AuthorShow> getShows() {
        return shows;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (null == o) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }

        if (!this.getName().trim().equals(((Presenter) o).getName().trim())) {
            return false;
        }else if (!this.getDob().equals(((Presenter) o).getDob())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash += hash * 31 + ((this.getName() == null) ? 0 : this.getName().hashCode());
        hash += hash * 31 + ((this.getDob() == null) ? 0 : this.getDob().hashCode());

        return hash;
    }
}
