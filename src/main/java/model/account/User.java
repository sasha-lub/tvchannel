package model.account;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "account")
@Access( AccessType.FIELD )
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    @NotBlank
    @Column(unique = true, nullable = false)
    @Length(max=30)
    @Pattern(regexp = "[A-ZА-ЯЁЇІa-zа-яёїіє' -`0-9_]*", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String login;

    @Pattern(regexp = ".{6,25}")
    @Column(nullable = false)
    private String password;

    @Pattern(regexp = "[A-ZА-ЯЁЇІa-zа-яёїіє' -`]*", flags = Pattern.Flag.CASE_INSENSITIVE)
    @Column
    private String name;

    @Email
    @NotBlank
    @Column(nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column
    private String imageUrl = "/resources/images/ava.jpg";

    public User() {
    }

    public User(String login, String password, String name, String email) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.email = email;
        this.role = Role.USER;
    }

//    @Override
//    public String toString() {
//        return "***User***" + System.lineSeparator()
//                + "login : " + login + System.lineSeparator()
//                + "password : " + password + System.lineSeparator()
//                + "name : " + name + System.lineSeparator()
//                + "e-mail : " + email + System.lineSeparator() +
//                "role : " + role + System.lineSeparator() + System.lineSeparator();
//    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (null == o) {
            return false;
        } else if (this.getClass() != o.getClass()) {
            return false;
        }

        if (!this.getEmail().equals(((User) o).getEmail())) {
            return false;
        }else if (!this.getLogin().equals(((User) o).getLogin())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash += hash * 31 + ((this.getEmail() == null) ? 0 : this.getEmail().hashCode());
        hash += hash * 31 + ((this.getLogin() == null) ? 0 : this.getLogin().hashCode());

        return hash;
    }

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}