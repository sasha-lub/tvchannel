package model.mediacontent;

import javax.persistence.Entity;

@Entity
public class Movie extends Show {
    public Movie(String name, String description) {
        super(name, description);
    }

    public Movie() {
    }
}