package model.mediacontent;

import javax.persistence.Entity;

@Entity
public class Series extends EpisodicShow {
	
	public Series() {
		super();
	}
	
	public Series(String name, String description) {
		super( name, description);
	}
}
