package model.mediacontent;

import model.Presenter;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Entity
public class AuthorShow extends EpisodicShow {

	@Valid
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "author_show_presenter", joinColumns = {
			@JoinColumn(name = "show_id", nullable = false, updatable = false)},
			inverseJoinColumns = { @JoinColumn(name = "presenter_id",
					nullable = false, updatable = false)})
	private Set<Presenter> presenters  = new HashSet<Presenter>();

	public AuthorShow() {
	}

//	@Override
//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		sb.append(super.toString());
//		if (!presenters.isEmpty()) {
//			sb.append("Presenters:").append(System.lineSeparator());
//			for (Presenter presenter : presenters) {
//				sb.append("\t").append(presenter);
//			}
//		}
//		return sb.toString();
//	}

	public AuthorShow(String name, String description) {
		super(name, description);
	}
	
	public void addPresenter(Presenter presenter){
		presenters.add(presenter);
	}

	public Set<Presenter> getPresenters() {
		return presenters;
	}
}
