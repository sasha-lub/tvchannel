package web;

import dto.UserDto;
import exception.DuplicatedNameException;
import exception.WrongLoginException;
import exception.WrongPasswordException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import service.UserService;
import web.constants.AttributeNames;
import web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Саня on 03.12.2016.
 */
@Controller
@RequestMapping(value = "account")
public class AccountController {

    @RequestMapping(value = "myAccount", method = RequestMethod.GET)
    public String viewAccount(
            HttpSession session,
            Map<String, Object> model){

        model.put(AttributeNames.AccountView.User, session.getAttribute("user"));
        return ViewNames.Account;
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity login(
            HttpSession session,
            Map<String, java.lang.Object> model,
            @RequestParam String login,
            @RequestParam String password){
        UserDto user = null;
        try {
                userService.login(login, password);
                user = userService.getByLogin(login);
                session.setAttribute("user", user);
                return new ResponseEntity(HttpStatus.OK);

        }catch (WrongLoginException e){
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }catch (WrongPasswordException e){
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @RequestMapping(value = "registration", method = RequestMethod.POST)
    public ResponseEntity registration(
            HttpSession session,
            String login,
            String password,
            String name,
            String email
    ){
        try{
            userService.add(login, password, name, email);
        }
        catch (DuplicatedNameException e){
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void logout(HttpSession session){
        session.removeAttribute("user");
    }

    @Inject
    UserService userService;
}
