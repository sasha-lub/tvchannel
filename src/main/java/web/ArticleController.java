package web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.ArticleService;
import web.constants.AttributeNames;
import web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Саня on 03.12.2016.
 */
@Controller
@RequestMapping(value = "/article")
public class ArticleController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String ViewArticle(
            HttpSession session,
            Map<String, Object> model,
            @RequestParam int articleId
    ){
      model.put(AttributeNames.ArticleView.Article, articleService.getById(articleId));
      return ViewNames.Article;
    }

    @Inject
    ArticleService articleService;
}
