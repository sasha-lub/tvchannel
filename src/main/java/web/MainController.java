package web;

import dto.ArticleDto;
import dto.PresenterDto;
import dto.ShowDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.*;
import web.constants.AttributeNames;
import web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by Саня on 03.12.2016.
 */
@Controller
@RequestMapping(path = "/main")
public class MainController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String handleRoot(HttpSession session, Map<String, java.lang.Object> model){
        model.put(AttributeNames.MainView.Articles,      getArticlesList());
        model.put(AttributeNames.MainView.Presenters,    getPresenterList());
        model.put(AttributeNames.MainView.Shows,         getShowsList());
        session.setAttribute("schedule", scheduleService.getAllShows());
        return ViewNames.Main;
    }

    private List<ShowDto> getShowsList(){
        return showService.getAll();
    }

    private List<ArticleDto> getArticlesList(){
        return articleService.getAll();
    }

    private List<PresenterDto> getPresenterList(){
        return presenterService.getAll();
    }

    @Inject
    @Qualifier("movieService")
    private MovieService showService;

    @Inject
    private ArticleService articleService;

    @Inject
    private PresenterService presenterService;

    @Inject
    private UserService userService;

    @Inject
    private ScheduleService scheduleService;
}