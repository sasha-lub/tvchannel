package web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.PresenterService;
import web.constants.AttributeNames;
import web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Саня on 03.12.2016.
 */
@Controller
@RequestMapping(value = "presenter")
public class PresenterController {
   @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewPresenter(
            HttpSession session,
            Map<String, Object> model,
            @RequestParam int presenterId){
       model.put(AttributeNames.PresenterView.Presenter, presenterService.getById(presenterId));
       return ViewNames.Presenter;
   }
    @Inject
    PresenterService presenterService;
}
