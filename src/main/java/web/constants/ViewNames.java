package web.constants;

/**
 * Created by Саня on 03.12.2016.
 */
public final class ViewNames
{
    public static final String Account      = "account";
    public static final String Article      = "article";
    public static final String Episode      = "episode";
    public static final String Error        = "error";
    public static final String Main         = "main";
    public static final String Presenter    = "presenter";
    public static final String Registration = "registration";
    public static final String Schedule     = "schedule";
    public static final String Show         = "show";
    public static final String Shows        = "shows";
}