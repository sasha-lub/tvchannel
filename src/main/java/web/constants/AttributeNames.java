package web.constants;

/**
 * Created by Саня on 03.12.2016.
 */
public final class AttributeNames {
    public static final String Message = "message";

    public static final class MainView{
        public static final String Shows = "shows";
        public static final String Articles = "articles";
        public static final String Presenters = "presenters";
    }

    public static final class ShowsView{
        public static final String Shows = "shows";
    }

    public static final class ShowView{
        public static final String Show = "show";
        public static final String Presenters = "presenters";
        public static final String Episodes = "episodes";
    }

    public static final class ArticleView{
        public static final String Article = "article";
    }

    public static final class PresenterView{
        public static final String Presenter = "presenter";
        public static final String Shows = "shows";
    }

    public static final class EpisodeView{
        public static final String Episode = "schedule";
    }

    public static final class ScheduleView{
        public static final String Shedule = "schedule";
    }

    public static final class ErrorView{
        public static final String Title  = "errorTitle";
        public static final String Message  = "errorMessage";
        public static final String Arguments  = "errorArguments";
    }

    public static final class AccountView{
        public static final String User = "user";
    }

    public static final class RegistrationView{
        public static final String Login = "login";
        public static final String Password = "password";
        public static final String ConfirmPassword = "confirmPassword";
        public static final String Email = "email";
        public static final String Name = "name";
        public static final String ImageUrl = "image";
    }


}
