package web;

import dto.ShowDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.MovieService;
import web.constants.ViewNames;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Саня on 03.12.2016.
 */

@Controller
@RequestMapping(value = "/show")
public class ShowController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String ViewShow(HttpSession session,
                           Map<String, Object> model,
                         @RequestParam int showId){
        ShowDto show = showService.getById(showId);
        model.put(ViewNames.Show, show);
        return ViewNames.Show;
    }

    @Inject
    @Qualifier("movieService")
    MovieService showService;

}
