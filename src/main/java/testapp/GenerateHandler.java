package testapp;

import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import service.*;

import javax.inject.Inject;

/**
 * Created by Саня on 10.11.2016.
 */
@Component
public class GenerateHandler {


    @Inject
    private ArticleService articleService;

    @Inject
    private UserService userService;

    @Inject
    @Qualifier("movieService")
    private MovieService movieService;

    @Inject
    @Qualifier("seriesService")
    private SeriesService seriesService;

    @Inject
    @Qualifier("authorShowService")
    private AuthorShowService authorShowService;

    @Inject
    private PresenterService presenterService;

    @Inject
    private ScheduleService schedule;

    public void run() throws DuplicatedNameException, NoSuchEntityException{
//        presenterService.add("Максим Доши", new LocalDate(1985, 7, 22),
//                "Что-то на уровне Высоцкого, а может даже и выше (с)мамка");
//        presenterService.changeImage(1, "/resources/images/presenters/doshi.jpg");
//
//        presenterService.add("Саша Грей", new LocalDate(1988, 11, 2),
//                "Что-то на уровне Высоцкого, а может даже и глубже (с)батя");
//        presenterService.changeImage(2, "/resources/images/presenters/sasha-grey.jpg");
//        presenterService.add("Гуф", new LocalDate(1977, 1, 21),
//                "Что-то на уровне Высоцкого, а может даже и мертвее (с)школьник");
//        presenterService.changeImage(3, "/resources/images/presenters/guf.jpg");
//
//        //////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        userService.add("xxxAndrYh@xxx", "password", "Андрей", "motherfucker123@mail.ru");
//        userService.add("admin", ")|(0p@", "Георгий", "georgiy.smeshnayafamilia@nure.ua");
//        userService.add("sasha-lub", "20022091712", "Александра", "sasha.lub@nure.ua");
//        userService.add("balash", "balashov", "Балаш", "balash.balash@nure.ua");
//
//
//        //////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        movieService.add(new Movie("Борат", "Телеведущий из Казахстана Борат отправляется в США, "
//                + "чтобы сделать репортаж об этой «величайшей в мире стране». "
//                + "Однако по прибытии оказалось, что главная цель его визита — "
//                + "поиски Памелы Андерсон с целью жениться на ней, а вовсе не " + "съемки документального фильма…"));
//        seriesService.add(new Series("Физрук",
//                "Главный герой Фома всю жизнь был «правой рукой» влиятельного "
//                        + "человека с полукриминальным прошлым. Когда «хозяин» "
//                        + "выгнал его на пенсию, Фома решил любым способом вернуться обратно."));
//        authorShowService.add(new AuthorShow("Званый ужин", "Кто из нас не мечтал пригласить своих гостей на званый ужин "
//                + "и создать незабываемую атмосферу праздника, уюта и " + "непринужденного общения?! "));
//        authorShowService.add(new AuthorShow("Дурнев +1",
//                "Не готовы к тонкому юмору и жесткому стебу? Оглядывайтесь по сторонам и удирайте!"));
//
//        seriesService.add(new Series("Family Guy","Сериал о типичной американской семье, " +
//                "где проживают самые обычные люди, способные разбить весь набор существующих стереотипов. Действительно, " +
//                "кого удивит обыкновенная говорящая собака? И совсем неудивительно, что песик иногда любит выкурить пару-другую " +
//                "сигарет и запить все это дело сухим мартини. Ну, а то, что бывают грудные младенцы с большими амбициями, " +
//                "так это – сплошь и рядом. Подумаешь, кроха мечтает поработить весь мир… При чем тут отклонение, спрашивается?.. " +
//                "Не сомневайтесь. Просто смотрите и умирайте от катарсиса в каждом эпизоде. Гриффины уже стучатся к вам, откройте!"));
//
//        movieService.add(new Movie("Star Wars IV","Татуин. Планета-пустыня. Уже постаревший рыцарь Джедай " +
//                "Оби Ван Кеноби спасает молодого Люка Скайуокера, когда тот пытается отыскать пропавшего дроида. " +
//                "С этого момента Люк осознает свое истинное назначение: он один из рыцарей Джедай. В то время как гражданская " +
//                "война охватила галактику, а войска повстанцев ведут бои против сил злого Императора, к Люку и Оби Вану " +
//                "присоединяется отчаянный пилот-наемник Хан Соло, и в сопровождении двух дроидов, " +
//                "R2D2 и C-3PO, этот необычный отряд отправляется на поиски предводителя повстанцев — принцессы Леи. "));
//
//        authorShowService.add(new AuthorShow("СМПУ","Наш канал и творческая команда проекта «Супермодель по-украински» " +
//                "готовит сюрприз всем поклонникам реалити! В четвертом сезоне кастинги будут открыты и для парней, и для девушек. " +
//                "Причем участников обоих полов на проекте будет поровну. И они абсолютно на равных смогут " +
//                "сражаться за главный приз и звание «Супермодели по-украински». Никаких команд – каждый сам за себя"));
//
//        authorShowService.add(new AuthorShow("Хата на тата","Хата на тата – проект посвящен женщинам, которые мечтают отдохнуть от утомительного быта. " +
//                "По условиям проекта, измученная мама едет на неделю в отпуск. Папа остается дома с детьми, "));
//
//        seriesService.add(new Series("Черное зеркало","За последние несколько лет новейшие технологии " +
//                "полностью изменили жизнь человечества, прежде чем мы сумели засомневаться в них. На сегодняшний день " +
//                "в каждом доме, квартире – присутствует плазменный телевизор, компьютер, телефон, ноутбук, различные " +
//                "виды дисплеев – одним словом черное зеркало двадцать первого века. Наша связь с реальностью начинает " +
//                "обретать новый характер. Мы прославили «Google» и «Apple» и дальнейшие их поколения. «Facebook», его " +
//                "алгоритм знает про нас больше чем мы сами или наши родственники. Мы имеем собственный доступ к " +
//                "информационным сетям, к информации обо всем мире но, к сожалению, в нашей голове хватает места лишь " +
//                "для сотни тысяч символов. В информационном мире отображается беспокойство за наш современный мир. Наша " +
//                "жизнь напоминает небольшой сериал, в котором между собой связаны два основных эпизода, зависимость людей " +
//                "от современных технологий."));
//
//        seriesService.add(new Series("Ольга","Ольга обычная русская женщина, которая одна " +
//                "«воспитывает» детей от разных браков, отца-алкоголика и бесшабашную сестру. При этом она остаётся " +
//                "жизнерадостной, сохраняет оптимизм и верит в «светлое» будущее."));
//        seriesService.add(new Series("Shameless","Американский сериал «Бесстыжие» представляет семейство Галлагеров, " +
//                "где старший – папаша Фрэнк, беспробудный пьяница, переложивший функции главы семьи на плечи дочери Фионе. " +
//                "Шестеро детей учатся жить самостоятельно, помогая друг другу выпутываться из бесконечных неприятностей. " +
//                "Мать семейства давно покинула дом, и с тех пор все ее обязанности: собирать младших в школу, делать все по " +
//                "хозяйству, разбираться с выходками Фрэнка приходится старшей дочери Фионе. Она забросила учебу, подрабатывает " +
//                "уборщицей и официанткой и еще строит свою личную жизнь."));
//
//        //////////////////////////////////////////////////////////////////////////////////////////////////////
//        ShowDto borat = movieService.getByName("Борат");
//        ShowDto dinner = authorShowService.getByName("Званый ужин");
//        ShowDto durnev = authorShowService.getByName("Дурнев +1");
//        ShowDto fizruk = seriesService.getByName("Физрук");
//        ShowDto sw = movieService.getByName("Star wars IV");
//        ShowDto smpu = authorShowService.getByName("СМПУ");
//        ShowDto hata = authorShowService.getByName("Хата на тата");
//        ShowDto griffin = seriesService.getByName("Family Guy");
//        ShowDto shame = seriesService.getByName("Shameless");
//
//        movieService.changeImage(borat.getId(), "/resources/images/show/borat.jpg");
//        movieService.changeImage(dinner.getId(), "/resources/images/show/dinner.jpg");
//        movieService.changeImage(durnev.getId(), "/resources/images/show/durnev+1.jpg");
//        movieService.changeImage(fizruk.getId(), "/resources/images/show/fizruk.jpeg");
//        movieService.changeImage(sw.getId(), "/resources/images/show/dart.jpg");
//        movieService.changeImage(smpu.getId(), "/resources/images/show/smpu.jpg");
//        movieService.changeImage(hata.getId(), "/resources/images/show/hata.jpg");
//        movieService.changeImage(griffin.getId(), "/resources/images/show/griffiny.jpg");
//        movieService.changeImage(shame.getId(), "/resources/images/show/shameless.jpg");
//
//
//        seriesService.addEpisode(fizruk.getId(), "Подстава", "физрука подставили", 1, 1);
//        seriesService.addEpisode(fizruk.getId(), "Разочаровние", "усачу не светит", 1, 2);
//        authorShowService.addEpisode(dinner.getId(), "Рэп от коляна",
//                "А Алена, Юлькина подруга\n\t" + "Вызывала у мужчин лишь чувство испуга.\n\t"
//                        + "Тощая, вертлявая, злая, рябая\n\t" + "Соблазнила-бы только Хана Мамая.\n\t"
//                        + "Юлька рядом с ней-сексуальная красавица\n\t" + "Любому олигарху и без пол-литра понравится\n\t"
//                        + "Жопа целлюлитная, но вполне аппетитная.\n\t" + "Грудь хоть и отвисла, но смотрится не кисло.", 1, 1);
//        authorShowService.addEpisode(dinner.getId(), "Гостепреимная Александра", "Саша развлекает троих гостей одновременно", 1, 2);
//        authorShowService.addEpisode(durnev.getId(), "Быдло в Балаклее", "Сотрудник Aldec показывает плохие результаты", 1, 1);
//        authorShowService.addEpisode(durnev.getId(), "Быдло в Краматорске", "Еще один сотрудник Aldec потерпел неудачу", 1, 2);
//
//        authorShowService.addPresenter(dinner.getId(), 1);
//        authorShowService.addPresenter(dinner.getId(), 2);
//        authorShowService.addPresenter(durnev.getId(), 3);
//        authorShowService.addPresenter(dinner.getId(), 3);
//        //////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        schedule.addItem(new LocalDate(2018, 4, 26), new LocalTime(13, 0, 0), new LocalTime(14, 50, 0),
//                fizruk.getId());
//        schedule.addItem(new LocalDate(2018, 4, 26), new LocalTime(19, 20, 0), new LocalTime(19, 50, 0),
//                fizruk.getId());
//        schedule.addItem(new LocalDate(2018, 4, 27), new LocalTime(19, 20, 0), new LocalTime(19, 50, 0),
//                fizruk.getId());
//        schedule.addItem(new LocalDate(2018, 4, 27), new LocalTime(19, 50, 0), new LocalTime(21, 30, 0),
//                dinner.getId());
//        schedule.addItem(new LocalDate(2018, 7, 28), new LocalTime(11, 20, 0), new LocalTime(12, 10, 0),
//                durnev.getId());
//        schedule.addItem(new LocalDate(2018, 8, 26), new LocalTime(13, 0, 0), new LocalTime(14, 50, 0),
//                borat.getId());
//        schedule.addItem(new LocalDate(2018, 2, 26), new LocalTime(19, 20, 0), new LocalTime(19, 50, 0),
//                fizruk.getId());
//        schedule.addItem(new LocalDate(2018, 11, 27), new LocalTime(19, 20, 0), new LocalTime(19, 50, 0),
//                fizruk.getId());
//        schedule.addItem(new LocalDate(2018, 6, 27), new LocalTime(19, 50, 0), new LocalTime(21, 30, 0),
//                dinner.getId());
//        schedule.addItem(new LocalDate(2018, 7, 28), new LocalTime(11, 20, 0), new LocalTime(12, 10, 0),
//                durnev.getId());
//
//        articleService.add("Что там у хохлов?", "все нормальненько");
//        articleService.add("Новый альбом Жанны Фриске",
//                "1.Ракировка\n" + "2.Я уеду жить в Ирак\n" + "3.Кто на горе свистнул\n"
//                        + "4.Press 'ok' or 'cancer'\n" + "5.Ктоооо проживает на дне океана?\n"
//                        + "6.Мои тиммейты");
//        articleService.add("ШОК! #aldec", "Константин Маслюк, в силу своей мудрости, лишился работы (зато не руки)");
//
//        userService.setRole(5, Role.ADMIN);
//        articleService.changeImage(35,"/resources/images/articles/salo.jpg");
//        articleService.changeImage(36,"/resources/images/articles/rak.jpg");
//        articleService.changeImage(37,"/resources/images/articles/aldec.png");

//        userService.changeImage(6, "/resources/images/users/sasha.jpg");
//        userService.changeImage(7, "/resources/images/users/balash.jpg");
//        userService.changeImage(38, "/resources/images/users/nastya.jpg");
        //////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
