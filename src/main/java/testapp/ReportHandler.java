package testapp;

import dto.*;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import service.*;
import service.impl.HibernateScheduleService;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Саня on 10.11.2016.
 */

@Component
public class ReportHandler {
    @Inject
    private UserService userService;

    @Inject
    private ArticleService articleService;

    @Inject
    @Qualifier("movieService")
    private MovieService movieService;

    @Inject
    @Qualifier("seriesService")
    private SeriesService seriesService;

    @Inject
    @Qualifier("authorShowService")
    private AuthorShowService authorShowService;

    @Inject
    private PresenterService presenterService;

    @Inject
    private ScheduleService schedule;

    public void run() {
        System.out.println("##########ARTICLES##########");

        List<ArticleDto> articles = articleService.getAll();
        for (ArticleDto article : articles) {
            System.out.println(article);
        }

        System.out.println("##########USERS##########");

        List<UserDto> users = userService.getAll();
        for (UserDto user : users) {
            System.out.println(user);
        }


        System.out.println("##########SCHEDULE##########");
        List<ScheduleItemDto> items = schedule.getAllShows();
        for (ScheduleItemDto item : items) {
            System.out.println(item);
        }

        System.out.println("##########PRESENTERS##########");

        List<PresenterDto> presenters = presenterService.getAll();
        for (PresenterDto presenter : presenters) {
            System.out.println(presenter);
        }
        System.out.println("##########SHOWS##########");

        List<ShowDto> shows = movieService.getAll();
        for (ShowDto show : shows) {
            System.out.println(show);
        }

        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        System.out.println(schedule.getShowsByDate(new LocalDate(2018, 4, 26)));
        System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    }
}
