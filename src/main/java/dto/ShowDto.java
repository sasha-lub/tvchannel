package dto;

import model.mediacontent.Genre;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Саня on 03.10.2016.
 */
public class ShowDto implements Serializable{
    private final Integer id;

    private final String name;

    private final List<Genre> genres;

    private final String description;

    private final  String url;

    private final String imageUrl;

    public String getName() {
        return name;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public Integer getId() {
        return id;
    }

    public ShowDto(Integer id, String name, String description, String url, List<Genre> genres, String imageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.genres = genres;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String toString() {
        return "***Show***" + System.lineSeparator()
                + name + System.lineSeparator()
                + description + System.lineSeparator();
    }
}
