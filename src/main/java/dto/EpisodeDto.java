package dto;

/**
 * Created by Саня on 03.10.2016.
 */
public class EpisodeDto {

    private final Integer id;

    private final String name;

    private final String description;

    private final int season;

    private final int episodeNumber;

    private final String imageUrl;

    public EpisodeDto(Integer id, String name, String description, int season, int episodeNumber, String imageUrl) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.season = season;
        this.episodeNumber = episodeNumber;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getSeason() {
        return season;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "***Episode***" + System.lineSeparator() +
                "\t" + "s" + season + "e" + episodeNumber + System.lineSeparator()
                + "\t" + name + System.lineSeparator()
                + "\t" + description + System.lineSeparator();
    }

}
