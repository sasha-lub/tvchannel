package dto;

import model.mediacontent.Genre;

import java.util.List;
import java.util.Set;

/**
 * Created by Саня on 03.10.2016.
 */
public class SeriesDto extends EpisodicShowDto {
    public SeriesDto(Integer id, String name, String description, String url, List<Genre> genres, Set<EpisodeDto> episodes, String image) {
        super(id, name, description, url, genres, episodes, image);
    }
}
