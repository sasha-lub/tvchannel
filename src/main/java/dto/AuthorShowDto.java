package dto;

import model.mediacontent.Genre;

import java.util.List;
import java.util.Set;

/**
 * Created by Саня on 03.10.2016.
 */
public class AuthorShowDto extends EpisodicShowDto {

    private final Set<PresenterDto> presenters;

    public AuthorShowDto(Integer id, String name, String description, String url, List<Genre> genres,
                         Set<EpisodeDto> episodes, Set<PresenterDto> presenters, String image) {
        super(id, name, description, url, genres, episodes, image);
        this.presenters = presenters;
    }

    public Set<PresenterDto> getPresenters() {
        return presenters;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        if (!presenters.isEmpty()) {
            sb.append("Presenters:").append(System.lineSeparator());
            for (PresenterDto presenter : presenters) {
                sb.append("\t").append(presenter);
            }
        }
        return sb.toString();
    }
}
