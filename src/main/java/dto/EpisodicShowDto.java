package dto;

import model.mediacontent.Genre;

import java.util.List;
import java.util.Set;

/**
 * Created by Саня on 03.10.2016.
 */
public class EpisodicShowDto extends ShowDto {
    private final Set<EpisodeDto> episodes;

    public Set<EpisodeDto> getEpisodes() {
        return episodes;
    }

    public EpisodicShowDto(Integer id, String name, String description, String url, List<Genre> genres, Set<EpisodeDto> episodes, String image) {
        super(id, name, description, url, genres, image);
        this.episodes = episodes;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        if (!episodes.isEmpty()) {
            sb.append("Episodes:").append(System.lineSeparator());
            for (EpisodeDto episode : episodes) {
                sb.append("\t").append(episode);
            }
        }
        return sb.toString();
    }

}
