package dto;

/**
 * Created by Саня on 03.10.2016.
 */
public class ArticleDto {

    private final Integer id;

    private final String header;

    private final String text;

    private final String imageUrl;

    public ArticleDto(Integer id, String header, String text, String imageUrl) {
        this.id = id;
        this.header = header;
        this.text = text;
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "***Article***" + System.lineSeparator()
                + header + System.lineSeparator()
                + text + System.lineSeparator() + System.lineSeparator();
    }

    public String getHeader() {
        return header;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getText() {
        return text;
    }

    public Integer getId() {
        return id;
    }
}
