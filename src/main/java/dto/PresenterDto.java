package dto;

import org.joda.time.LocalDate;

import java.util.Set;

/**
 * Created by Саня on 03.10.2016.
 */
public class PresenterDto {
    public PresenterDto(Integer id, String name, LocalDate dob, String description, String imageUrl) {
        this.id = id;
        this.name = name;
        this.dob = dob;
        this.description = description;
        this.imageUrl = imageUrl;
    //    this.shows = shows;
    }
    private final Integer id;

    private final String name;

    private final LocalDate dob;

    private final String description;

    private final String imageUrl;

    // private final Set<AuthorShowDto> shows;

    public String getName() {
        return name;
    }

    public LocalDate getDob() {
        return dob;
    }

    public String getDescription() {
        return description;
    }

    public Integer getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }


//    public Set<AuthorShowDto> getShows() {
//        return shows;
//    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("***Presenter***").append(System.lineSeparator())
                .append("name : ").append(name)
                .append(", date of birth : ").append(dob.toString("dd.MM.yyyy"))
                .append(System.lineSeparator()).append(description).append(System.lineSeparator());
//        if (shows != null && !shows.isEmpty()) {
//            for (AuthorShowDto show : shows) {
//                sb.append(show.getName()).append(System.lineSeparator());
//            }
//        }
        sb.append(System.lineSeparator());
        return sb.toString();
    }
}
