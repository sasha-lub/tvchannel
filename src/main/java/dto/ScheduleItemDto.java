package dto;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.io.Serializable;

/**
 * Created by Саня on 03.10.2016.
 */
public class ScheduleItemDto implements Serializable{

    private final Integer id;

    private final LocalDate date;

    private final LocalTime startTime;

    private final LocalTime endTime;

    private final String showName;

    private final int showId;

    public ScheduleItemDto(Integer id, LocalDate date, LocalTime startTime, LocalTime endTime, String showName, int showId) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.showName = showName;
        this.showId = showId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("***ScheduleItem***")
                .append(System.lineSeparator())
                .append("date : ")
                .append((date).toString("dd.MM.yyyy"))
                .append(System.lineSeparator())
                .append("time : ")
                .append((startTime).toString("HH:mm:ss"))
                .append(" - ")
                .append((endTime).toString("HH:mm:ss"))
                .append("show ").append(showName)
                .append(System.lineSeparator());

        return sb.toString();
    }
    public Integer getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public String getShowName() {
        return showName;
    }

    public int getShowId() {
        return showId;
    }
}
