package dto;

import model.mediacontent.Genre;

import java.util.List;

/**
 * Created by Саня on 03.10.2016.
 */
public class MovieDto extends ShowDto {
    public MovieDto(Integer id, String name, String description, String url, List<Genre> genres, String image) {
        super(id, name, description, url, genres, image);
    }
}
