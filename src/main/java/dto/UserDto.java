package dto;

import model.account.Role;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Саня on 03.10.2016.
 */
public class UserDto implements Serializable{
    private final Integer id;

    private final String name;

    private final String login;

    private final Role role;

    private final String email;

    private final String imageUrl;

    public UserDto(Integer id, String name, String email, String imageUrl, String login, Role role) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.imageUrl = imageUrl;
        this.login = login;
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLogin() {
        return login;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "***User***" + System.lineSeparator()
                + "name : " + name + System.lineSeparator()
                + "e-mail : " + email + System.lineSeparator();
    }
}
