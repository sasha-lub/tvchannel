package dao;

import model.Presenter;

/**
 * Created by sasha on 04.05.2016.
 */
public interface IPresenterDao extends IDAO<Presenter> {

}