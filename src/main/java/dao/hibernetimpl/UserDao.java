package dao.hibernetimpl;

import dao.IUserDao;
import model.account.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */

@Repository
public class UserDao extends DAO<User> implements IUserDao{

    public User getByEmail(String email) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root)
                .where(cb
                        .equal(root
                                .get( "email" ),email));

        List<User> result = getEntityManager().createQuery(criteria).getResultList();
        return result.size()==1?result.get(0):null;
    }

    public User getByLogin(String login) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> root = criteria.from(User.class);
        criteria.select(root)
                .where(cb
                        .equal(root
                                .get( "login" ),login));
        List<User> result = getEntityManager().createQuery(criteria).getResultList();
        return result.size()==1?result.get(0):null;
    }


}