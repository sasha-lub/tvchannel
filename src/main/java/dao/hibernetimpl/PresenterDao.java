package dao.hibernetimpl;

import dao.IPresenterDao;
import model.Presenter;
import org.springframework.stereotype.Repository;

/**
 * Created by sasha on 04.05.2016.
 */

@Repository
public class PresenterDao extends DAO<Presenter> implements IPresenterDao{

}