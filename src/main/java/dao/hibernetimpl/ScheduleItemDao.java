package dao.hibernetimpl;

import dao.IScheduleItemDao;
import model.ScheduleItem;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */

@Repository
public class ScheduleItemDao extends DAO<ScheduleItem> implements IScheduleItemDao {

    public List<ScheduleItem> getByDate(LocalDate date) {

        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ScheduleItem> criteria = criteriaBuilder.createQuery(ScheduleItem.class);
        Root<ScheduleItem> items = criteria.from(ScheduleItem.class);
        Predicate condition = criteriaBuilder.equal(items.get("date"), date);
        criteria.where(condition);

        return getEntityManager().createQuery(criteria).getResultList();
    }
}