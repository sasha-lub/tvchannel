package dao.hibernetimpl;

import dao.IEpisodeDao;
import model.mediacontent.Episode;
import org.springframework.stereotype.Repository;

/**
 * Created by sasha on 04.05.2016.
 */

@Repository
public class EpisodeDao extends DAO<Episode> implements IEpisodeDao{

}