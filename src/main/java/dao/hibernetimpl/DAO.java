package dao.hibernetimpl;

import dao.IDAO;
import org.hibernate.Session;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import persistance.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public abstract class DAO<T> implements IDAO<T> {

    @PersistenceContext
    private EntityManager entityManager;

    protected EntityManager getEntityManager (){
        return this.entityManager;
    }

    public void add(T entity) {
        this.entityManager.persist(entity);
    }

    public T getById(Integer id, Class<T> clazz) {
        return this.entityManager.find(clazz, id);
    }

    public void remove(T entity) {
       this.entityManager.remove(entity);
    }

    public List<T> getAll(Class<T> clazz) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery< T > criteria = cb.createQuery(clazz);
        criteria.select(criteria.from(clazz));
        return entityManager.createQuery( criteria ).getResultList();
    }
}
