package dao.hibernetimpl;

import dao.IShowDao;
import model.mediacontent.Show;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ShowDao extends DAO<Show> implements IShowDao{

    public Show getByName(String name) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Show> criteria = cb.createQuery(Show.class);
        Root<Show> root = criteria.from(Show.class);
        criteria.select(root)
                .where(cb
                        .equal(root
                                .get( "name" ),name));

        List<Show> result = getEntityManager().createQuery(criteria).getResultList();
        return result.size()==1?result.get(0):null;
    }
}