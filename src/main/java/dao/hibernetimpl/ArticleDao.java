package dao.hibernetimpl;

import dao.IArticleDao;
import model.Article;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */

@Repository
public class ArticleDao extends DAO<Article> implements IArticleDao{

    public Article getByHeader(String header) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Article> criteria = cb.createQuery(Article.class);
        Root<Article> root = criteria.from(Article.class);
        criteria.select(root)
                .where(cb
                        .equal(root
                                .get( "header" ),header));
        List<Article> result = getEntityManager().createQuery(criteria).getResultList();
        return result.size()==1?result.get(0):null;
    }
}