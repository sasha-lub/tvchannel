package dao;

import model.Article;

/**
 * Created by sasha on 04.05.2016.
 */
public interface IArticleDao extends IDAO<Article> {

    Article getByHeader(String header);
}