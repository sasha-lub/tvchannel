package dao;

import model.mediacontent.Episode;

/**
 * Created by sasha on 04.05.2016.
 */
public interface IEpisodeDao extends IDAO<Episode> {

}