package dao;

import model.account.User;

/**
 * Created by sasha on 04.05.2016.
 */
public interface IUserDao extends IDAO<User> {

    User getByEmail(String email);

    User getByLogin(String login);
}