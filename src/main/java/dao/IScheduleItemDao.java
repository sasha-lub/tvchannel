package dao;

import model.ScheduleItem;
import org.joda.time.LocalDate;
import java.util.List;

/**
 * Created by sasha on 04.05.2016.
 */
public interface IScheduleItemDao extends IDAO<ScheduleItem> {

    List<ScheduleItem> getByDate(LocalDate date);
}