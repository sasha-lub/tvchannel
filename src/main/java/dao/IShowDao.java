package dao;

import model.mediacontent.Show;

public interface IShowDao extends IDAO<Show> {

    Show getByName(String name);
}