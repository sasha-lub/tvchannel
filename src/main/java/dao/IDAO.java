package dao;

import java.util.List;

public interface IDAO<T> {

    void add(T entity);

    T getById(Integer id, Class<T> clazz);

    void remove(T entity);

    List<T> getAll(Class<T> clazz);
}
