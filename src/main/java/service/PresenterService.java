package service;

import dto.PresenterDto;
import exception.NoSuchEntityException;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.List;

/**
 * Created by sasha on 08.05.2016.
 */
public interface PresenterService {

    void add(
            @NotBlank String name,
            @Past @NotNull LocalDate dob,
            @NotBlank String description)
;
    PresenterDto getById(
            @NotNull Integer id)
            throws NoSuchEntityException
;
    void remove(
            @NotNull Integer id)
;
    void changeName(
            @NotNull Integer id,
            @NotBlank String newName)
            throws NoSuchEntityException
;
    void changeDescription(
            @NotNull Integer id,
            @NotBlank String newDescription)
            throws NoSuchEntityException
;
    void changeImage(
            @NotNull Integer id,
            String url)
            throws NoSuchEntityException
            ;
    List<PresenterDto> getAll();
}

