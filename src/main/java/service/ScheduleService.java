package service;

import dto.ScheduleItemDto;
import exception.NoSuchEntityException;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Саня on 11.11.2016.
 */
public interface ScheduleService {

    public List<ScheduleItemDto> getAllShows();

    public List<ScheduleItemDto> getShowsByDate(
            @NotNull LocalDate date);

    public void addItem(
            @Future LocalDate date,
            @NotNull LocalTime startTime,
            @NotNull LocalTime endTime,
            @NotNull Integer showId)
            throws NoSuchEntityException
            ;

}
