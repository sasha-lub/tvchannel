package service;

import dto.ShowDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.mediacontent.Show;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface MovieService {

    void add(
            @NotNull Show show)
            throws DuplicatedNameException
;
    ShowDto getById(
            @NotNull Integer id)
            throws NoSuchEntityException
;
    void remove(
            @NotNull Integer id)
            throws NoSuchEntityException
;
    void changeDescription(
            @NotNull Integer id,
            @NotBlank String description)
            throws NoSuchEntityException
            ;

    void changeImage(
            @NotNull Integer id,
            String url)
            throws NoSuchEntityException
            ;
    List<ShowDto> getAll();

    ShowDto getByName(String name)
            throws NoSuchEntityException
;
}
