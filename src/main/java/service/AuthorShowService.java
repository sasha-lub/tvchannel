package service;

import exception.NoSuchEntityException;

import javax.validation.constraints.NotNull;

public interface AuthorShowService extends SeriesService {

    void addPresenter(
            @NotNull Integer showId,
            @NotNull Integer presenterId)
            throws NoSuchEntityException;
}
