package service.impl;

import dao.IPresenterDao;
import dao.IShowDao;
import exception.NoSuchEntityException;
import model.Presenter;
import model.mediacontent.AuthorShow;
import model.mediacontent.Show;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.AuthorShowService;

import javax.inject.Inject;

@Service(value = "authorShowService")
public class HibernateAuthorShowService extends HibernateSeriesService implements AuthorShowService {
    public HibernateAuthorShowService() {
    }

    @Inject
    IShowDao showDao;

    @Inject
    IPresenterDao presenterDao;

    @Transactional
    @Override
    public void addPresenter(Integer showId, Integer presenterId) throws NoSuchEntityException{
        AuthorShow show = (AuthorShow) showDao.getById(showId, Show.class);
        if(show == null){
            throw new NoSuchEntityException(AuthorShow.class, showId);
        }

        Presenter presenter = presenterDao.getById(presenterId, Presenter.class);
        if(presenter == null){
            throw new NoSuchEntityException(Presenter.class, presenterId);
        }

        show.addPresenter(presenter);
    }
}
