package service.impl;

import dto.*;
import model.Article;
import model.Presenter;
import model.ScheduleItem;
import model.account.User;
import model.mediacontent.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Саня on 04.10.2016.
 */
public final class DtoBuilder<T> {
    public static UserDto toDto(User user){
        return new UserDto(user.getId(), user.getName(), user.getEmail(), user.getImageUrl(), user.getLogin(), user.getRole());
    }
    public static ArticleDto toDto(Article article){
        return new ArticleDto(article.getId(),article.getHeader(), article.getText(), article.getImageUrl());
    }
    public static AuthorShowDto toDto(AuthorShow show){


        Set<PresenterDto> presenterDtos = new HashSet<>();
        Set<ShowDto> showDtos = new HashSet<>();

        show.getPresenters().stream().forEach(

                presenter -> presenterDtos.add(
                        new PresenterDto(
                                presenter.getId(),
                                presenter.getName(),
                                presenter.getDob(),
                                presenter.getDescription(),
                                presenter.getImageUrl()
                        )
                )
        );
        return new AuthorShowDto(show.getId(), show.getName(), show.getDescription(), show.getUrl(), show.getGenres(), getEpisodes(show), presenterDtos, show.getImageUrl());
    }
    public static EpisodeDto toDto(Episode episode){
        return new EpisodeDto(episode.getId(), episode.getName(), episode.getDescription(), episode.getSeason(), episode.getEpisodeNumber(), episode.getImageUrl());
    }
    public static EpisodicShowDto toDto(EpisodicShow show){
        return new EpisodicShowDto(show.getId(), show.getName(), show.getDescription(), show.getUrl(), show.getGenres(), getEpisodes(show), show.getImageUrl());
    }
    public static MovieDto toDto(Movie movie){
        return new MovieDto(movie.getId(), movie.getName(), movie.getDescription(), movie.getUrl(), movie.getGenres(), movie.getImageUrl());
    }
    public static PresenterDto toDto(Presenter presenter){
        return new PresenterDto(presenter.getId(), presenter.getName(), presenter.getDob(), presenter.getDescription(), presenter.getImageUrl());
    }
    public static ScheduleItemDto toDto(ScheduleItem item){
        return new ScheduleItemDto(item.getId(), item.getDate(), item.getStartTime(), item.getEndTime(), item.getShow().getName(), item.getShow().getId());
    }
    public static SeriesDto toDto(Series series){

        return new SeriesDto(series.getId(), series.getName(), series.getDescription(), series.getUrl(), series.getGenres(), getEpisodes(series), series.getImageUrl());
    }
    public static ShowDto toDto(Show show){
        if(show.getClass().equals(Series.class)){
            return toDto((Series) show);
        }else if(show.getClass().equals(AuthorShow.class)){
            return toDto((AuthorShow) show);
        }else{
            return toDto((Movie) show);
        }
    }

    private static Set<EpisodeDto> getEpisodes(EpisodicShow show){
        Set<EpisodeDto> episodeDtos = new HashSet<>();
        show.getEpisodes().stream().forEach(
                episode -> episodeDtos.add(
                        new EpisodeDto(
                                episode.getId(),
                                episode.getName(),
                                episode.getDescription(),
                                episode.getSeason(),
                                episode.getEpisodeNumber(),
                                episode.getImageUrl()
                        )
                )
        );
        return episodeDtos;
    }
}
