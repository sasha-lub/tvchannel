package service.impl;

import dao.IArticleDao;
import dto.ArticleDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.Article;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.ArticleService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
public class HibernateArticleService implements ArticleService {
    public HibernateArticleService() {
    }

    @Inject
    IArticleDao dao;

    @Transactional
    @Override
    public void add(String header, String text) throws DuplicatedNameException {
        if(getByHeader(header) != null){
            throw new DuplicatedNameException(Article.class, header);
        }

        dao.add(new Article(header, text));
    }

    @Transactional
    @Override
    public ArticleDto getById(Integer id) throws NoSuchEntityException{
        Article article = (Article)dao.getById(id, Article.class);
        if(article == null){
            throw new NoSuchEntityException(Article.class, id);
        }
        return DtoBuilder.toDto(article);
    }

    @Transactional
    @Override
    public void remove(Integer id) {
        dao.remove(dao.getById(id, Article.class));
    }

    @Transactional
    @Override
    public void changeHeader(Integer id, String newHeader) throws NoSuchEntityException, DuplicatedNameException {

        Article a = dao.getById(id, Article.class);
        if(a == null){
            throw new NoSuchEntityException(Article.class, id);
        }else if(getByHeader(newHeader) != null){
            throw new DuplicatedNameException(Article.class, newHeader);
        }
        a.setHeader(newHeader);
    }

    @Transactional
    @Override
    public void changeText(Integer id, String newText) throws NoSuchEntityException {

        Article a = dao.getById(id, Article.class);
        if(a == null){
            throw new NoSuchEntityException(Article.class, id);
        }
        a.setText(newText);
    }

    @Transactional
    @Override
    public void changeImage(@NotNull Integer id, @NotBlank String url) throws NoSuchEntityException {
        Article a = dao.getById(id, Article.class);
        if(a == null){
            throw new NoSuchEntityException(Article.class, id);
        }
        a.setImageUrl(url);
    }

    @Transactional
    @Override
    public List<ArticleDto> getAll() {

        List<ArticleDto> articleDtos = new ArrayList<ArticleDto>();
        List<Article> articles = dao.getAll(Article.class);
        for (Article article : articles) {
            articleDtos.add(DtoBuilder.toDto(article));
        }

        return articleDtos;
    }

    @Transactional
    @Override
    public ArticleDto getByHeader(String header) throws NoSuchEntityException{

        Article article = (Article)dao.getByHeader(header);
        if(article == null){
            return null;
        }
        return DtoBuilder.toDto(article);
    }
}
