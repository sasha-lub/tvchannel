package service.impl;

import dao.IPresenterDao;
import dto.PresenterDto;
import exception.NoSuchEntityException;
import model.Presenter;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.PresenterService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sasha on 08.05.2016.
 */

@Service
public class HibernatePresenterService implements PresenterService {
    public HibernatePresenterService() {}

    @Inject
    IPresenterDao dao;

    @Transactional
    @Override
    public void add(String name, LocalDate dob, String description) {
        dao.add(new Presenter(name, dob, description));
    }

    @Transactional
    @Override
    public PresenterDto getById(Integer id) throws NoSuchEntityException {
        Presenter p = dao.getById(id, Presenter.class);
        if(p == null){
            throw new NoSuchEntityException(Presenter.class, id);
        }
        return DtoBuilder.toDto(p);
    }

    @Transactional
    @Override
    public void remove(Integer id) {
        dao.remove(dao.getById(id, Presenter.class));
    }

    @Transactional
    @Override
    public void changeName(Integer id, String newName) throws NoSuchEntityException{
        Presenter p = dao.getById(id, Presenter.class);
        if(p == null){
            throw new NoSuchEntityException(Presenter.class, id);
        }
        p.setName(newName);
    }

    @Transactional
    @Override
    public void changeDescription(Integer id, String newDescription) throws NoSuchEntityException{
        Presenter p = dao.getById(id, Presenter.class);
        if(p == null){
            throw new NoSuchEntityException(Presenter.class, id);
        }
        p.setDescription(newDescription);
    }

    @Transactional
    @Override
    public void changeImage(@NotNull Integer id, String url) throws NoSuchEntityException {
        Presenter presenter = dao.getById(id, Presenter.class);
        if(presenter == null){
            throw new NoSuchEntityException(Presenter.class, id);
        }
        presenter.setImageUrl(url);
    }

    @Transactional
    @Override
    public List<PresenterDto> getAll() {
        List<PresenterDto> presenterDtos = new ArrayList<>();
        List<Presenter> presenters = dao.getAll(Presenter.class);
        for (Presenter presenter : presenters) {
            presenterDtos.add(DtoBuilder.toDto(presenter));
        }
        return presenterDtos;
    }
}
