package service.impl;

import dao.IEpisodeDao;
import dao.IShowDao;
import exception.NoSuchEntityException;
import model.mediacontent.Episode;
import model.mediacontent.EpisodicShow;
import model.mediacontent.Show;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.SeriesService;

import javax.inject.Inject;

@Service(value = "seriesService")
public class HibernateSeriesService extends HibernateMovieService implements SeriesService {
    public HibernateSeriesService() {}

    @Inject
    IShowDao showDao;

    @Inject
    IEpisodeDao episodeDao;

    @Transactional
    @Override
    public void addEpisode(Integer seriesId, String name, String description, int season, int episodeNumber) {
        EpisodicShow series = (EpisodicShow) showDao.getById(seriesId, Show.class);
        if(series == null){
            throw new NoSuchEntityException(EpisodicShow.class, "No entity with id = "+seriesId);
        }
        Episode episode = new Episode(name, description, season, episodeNumber);
        series.addEpisode(episode);
        episodeDao.add(episode);
    }
}
