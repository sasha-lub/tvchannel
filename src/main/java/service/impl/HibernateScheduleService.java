package service.impl;

import dao.IScheduleItemDao;
import dao.IShowDao;
import dto.ScheduleItemDto;
import exception.NoSuchEntityException;
import model.ScheduleItem;
import model.mediacontent.Show;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.ScheduleService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
public class HibernateScheduleService implements ScheduleService{
	public HibernateScheduleService() {}

	@Inject
	private IScheduleItemDao dao;

	@Inject
	private IShowDao showDao;

	@Transactional
	public List<ScheduleItemDto> getAllShows() {
		return getDtos(dao.getAll(ScheduleItem.class));
	}

	@Transactional
	public List<ScheduleItemDto> getShowsByDate(LocalDate date) {
		return getDtos(dao.getByDate(date));
    }

	@Transactional
	public void addItem(LocalDate date, LocalTime startTime, LocalTime endTime, Integer showId) throws NoSuchEntityException {
		Show show = showDao.getById(showId, Show.class);
		if(show == null){
			throw new NoSuchEntityException(Show.class, showId);
		}
        dao.add(new ScheduleItem(date, startTime, endTime, show));
	}

	private static List<ScheduleItemDto> getDtos(List<ScheduleItem> items){
		List<ScheduleItemDto> dtos = new ArrayList<>();
		items.stream().forEach(item -> {dtos.add(DtoBuilder.toDto(item));});
		return dtos;
	}

	@Override
	public String toString() {
        List<ScheduleItemDto> items = getAllShows();
		StringBuilder sb = new StringBuilder();
		if (!items.isEmpty()){
			sb.append("##########SCHEDULE##########").append(System.lineSeparator());
			for (ScheduleItemDto scheduleItem : items) {
				sb.append(scheduleItem).append(System.lineSeparator());
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
}