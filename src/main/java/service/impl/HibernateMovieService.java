package service.impl;

import dao.IShowDao;
import dto.ShowDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import model.mediacontent.Show;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.MovieService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service(value = "movieService")
public class HibernateMovieService implements MovieService {

    public HibernateMovieService() {}

    @Inject
    private IShowDao dao;

    @Transactional
    @Override
    public void add(Show show) throws DuplicatedNameException{
        Show s = dao.getByName(show.getName());
        if(s != null){
            throw new DuplicatedNameException(Show.class, show.getName());
        }
        dao.add(show);
    }

    @Transactional
    @Override
    public ShowDto getById(Integer id) throws NoSuchEntityException{
        Show show = dao.getById(id, Show.class);
        if (show == null){
            throw new NoSuchEntityException(Show.class, id);
        }
        return DtoBuilder.toDto(show);
    }

    @Transactional
    @Override
    public void remove(Integer id) {
        dao.remove(dao.getById(id, Show.class));
    }

    @Transactional
    @Override
    public void changeDescription(Integer id, String description) throws NoSuchEntityException{
        Show show = dao.getById(id, Show.class);
        if (show == null){
            throw new NoSuchEntityException(Show.class, id);
        }
        show.setDescription(description);
    }

    @Transactional
    @Override
    public void changeImage(@NotNull Integer id, String url) throws NoSuchEntityException {
        Show s = dao.getById(id, Show.class);
        if(s == null){
            throw new NoSuchEntityException(Show.class, id);
        }
        s.setImageUrl(url);
    }

    @Transactional
    @Override
    public List<ShowDto> getAll() {
        return getDtos(dao.getAll(Show.class));
    }

    @Transactional
    @Override
    public ShowDto getByName(String name) throws NoSuchEntityException{
        Show show = dao.getByName(name);
        if (show == null){
            throw new NoSuchEntityException(Show.class, name);
        }
        return DtoBuilder.toDto(show);
    }

    private static List<ShowDto> getDtos(List<Show> items){
        List<ShowDto> dtos = new ArrayList<>();
        items.stream().forEach(item -> dtos.add(DtoBuilder.toDto(item)));
        return dtos;
    }
}