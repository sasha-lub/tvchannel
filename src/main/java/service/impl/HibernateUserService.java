package service.impl;

import dao.IUserDao;
import dto.UserDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import exception.WrongLoginException;
import exception.WrongPasswordException;
import model.account.Role;
import model.account.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import service.UserService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
public class HibernateUserService implements UserService {
    public HibernateUserService() {}

    @Inject
    IUserDao dao;

    @Transactional
    @Override
    public void add(String login,  String password, String name, String email) throws DuplicatedNameException {
        User user = dao.getByLogin(login);
        if (user!=null){
            throw new DuplicatedNameException(User.class, login);
        }
        user = dao.getByEmail(email);
        if (user!=null){
            throw new DuplicatedNameException(User.class, email);
        }
        dao.add(new User(login, password, name, email));
    }

    @Transactional
    @Override
    public UserDto getById(Integer id) throws NoSuchEntityException {
        User user = dao.getById(id, User.class);
        if (user == null){
            throw new NoSuchEntityException(User.class, id);
        }
        return DtoBuilder.toDto(user);
    }

    @Transactional
    @Override
    public void remove(Integer id) {
        dao.remove(dao.getById(id, User.class));
    }

    @Transactional
    @Override
    public boolean login(String login, String password) throws WrongLoginException, WrongPasswordException {

        User user = dao.getByLogin(login);
        if (user == null) {
            throw new WrongLoginException(login);
        }
        else{
            if(!user.getPassword().equals(password)){
                throw new WrongPasswordException(login);
            }else {
                return true;
            }
        }
    }

    @Transactional
    @Override
    public void changeLogin(Integer id, String newLogin) throws DuplicatedNameException {
        if(getByLogin(newLogin) != null){
            throw new DuplicatedNameException(User.class, "Login is already in use");
        }
        User user = dao.getById(id, User.class);
        if(user == null){
            throw new NoSuchEntityException(User.class, id);
        }
        user.setLogin(newLogin);
    }

    @Transactional
    @Override
    public void changePassword(Integer id, String oldPassword, String newPassword) throws WrongPasswordException {
        User user = dao.getById(id, User.class);
        if(user == null){
            throw new NoSuchEntityException(User.class, id);
        }else if(user.getPassword().equals(oldPassword)){
        user.setPassword(newPassword);
        }else {
            throw new WrongPasswordException(user.getLogin());
        }
    }

    @Transactional
    @Override
    public void changeEmail(Integer id, String newEmail) throws DuplicatedNameException {
        if(getByEmail(newEmail) != null){
            throw new DuplicatedNameException(User.class, "Email is already in use");
        }
        User user = dao.getById(id, User.class);
        if(user == null){
            throw new NoSuchEntityException(User.class, id);
        }
        user.setEmail(newEmail);
    }

    @Transactional
    @Override
    public void changeName(Integer id, String newName) {
        User user = dao.getById(id, User.class);
        if(user == null){
            throw new NoSuchEntityException(User.class, id);
        }
        user.setName(newName);
    }

    @Transactional
    @Override
    public void setRole(Integer id, Role role) throws NoSuchEntityException {
        User user = dao.getById(id, User.class);
        if (user == null){
            throw new NoSuchEntityException(User.class, id);
        }
        user.setRole(role);
    }

    @Transactional
    @Override
    public void changeImage(@NotNull Integer id, String url) throws NoSuchEntityException {
        User user = dao.getById(id, User.class);
        if(user == null){
            throw new NoSuchEntityException(User.class, id);
        }
        user.setImageUrl(url);
    }

    @Transactional
    @Override
    public List<UserDto> getAll() {

        List<UserDto> userDtos = new ArrayList<>();

        dao.getAll(User.class).stream().forEach(
                user -> userDtos.add(
                        new UserDto(
                                user.getId(),
                                user.getName(),
                                user.getEmail(),
                                user.getImageUrl(),
                                user.getLogin(),
                                user.getRole()
                        )
                )
        );

        return userDtos;
    }

    @Transactional
    @Override
    public UserDto getByEmail(String email) throws NoSuchEntityException {
        User u = dao.getByEmail(email);
        if (u == null){
            return null;
        }
        return DtoBuilder.toDto(u);
    }

    @Transactional
    @Override
    public UserDto getByLogin(String login) throws NoSuchEntityException {
        User u = dao.getByLogin(login);
        if (u == null){
            return null;
        }
        return DtoBuilder.toDto(u);
    }
}