package service;

import dto.ArticleDto;
import exception.DuplicatedNameException;
import exception.NoSuchEntityException;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by sasha on 08.05.2016.
 */

public interface ArticleService {
    void add(
            @NotBlank String header,
            @NotBlank String text) throws DuplicatedNameException
            ;

    ArticleDto getById(
            @NotNull Integer id)
            throws NoSuchEntityException
            ;

    void remove(
            @NotNull Integer id)
            throws NoSuchEntityException
            ;
    void changeHeader(
            @NotNull Integer id,
            @NotNull String newHeader)
            throws NoSuchEntityException, DuplicatedNameException
            ;
    void changeText(
            @NotNull Integer id,
            @NotBlank String newText)
            throws NoSuchEntityException
            ;
    void changeImage(
            @NotNull Integer id,
            String url)
            throws NoSuchEntityException
            ;
    ArticleDto getByHeader(String header)
            throws NoSuchEntityException;

    List<ArticleDto> getAll();
}
