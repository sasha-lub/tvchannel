package service;

import exception.*;
import model.account.Role;
import dto.UserDto;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

public interface UserService {
    void add(
            @NotBlank String login,
            @NotBlank @Pattern(regexp = ".{6,25}") String password,
            @NotBlank @Pattern(regexp = "[A-ZА-ЯЁЇІЄa-zа-яёїіє]") String name,
            @Email String email)
            throws DuplicatedNameException
;
    UserDto getById(
            @NotNull Integer id)
            throws NoSuchEntityException
;
    void remove(
            @NotNull Integer id)
;
    boolean login(
            @NotBlank String login,
            @NotBlank String password)
            throws WrongLoginException, WrongPasswordException;
;
    void changeLogin(
            @NotNull Integer id,
            @NotBlank @Length(max = 30) String newLogin)
            throws NoSuchEntityException, DuplicatedNameException
;
    void changePassword(
            @NotNull Integer id,
            @NotBlank String oldPassword,
            @NotBlank @Pattern(regexp = "[A-ZА-ЯЁЇІЄa-zа-яёїіє0-9]{6,25}") String newPassword)
            throws NoSuchEntityException, WrongPasswordException
;
    void changeEmail(
            @NotNull Integer id,
            @Email String newEmail)
            throws NoSuchEntityException, DuplicatedNameException;
;
    void changeName(
            @NotNull Integer id,
            @NotBlank @Pattern(regexp = "[A-ZА-ЯЁЇІЄa-zа-яёїіє]") String newName)
            throws NoSuchEntityException
;
    void setRole(
            @NotNull Integer id,
            @NotNull Role role)
            throws NoSuchEntityException;
;

    void changeImage(
            @NotNull Integer id,
            String url)
            throws NoSuchEntityException
            ;

    List<UserDto> getAll();

    UserDto getByEmail(String email)
            throws NoSuchEntityException;
;
    UserDto getByLogin(String login)
            throws NoSuchEntityException;
;
}
