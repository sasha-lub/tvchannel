package service;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public interface SeriesService extends MovieService {

    void addEpisode(
            @NotNull Integer seriesId,
            @NotBlank String name,
            @NotBlank String description,
            @Min(value = 1) int season,
            @Min(value = 0) int episodeNumber)
;
}
