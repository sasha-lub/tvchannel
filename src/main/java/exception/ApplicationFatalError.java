package exception;

/**
 * Created by Саня on 15.11.2016.
 */
public class ApplicationFatalError extends RuntimeException {
public ApplicationFatalError(){
    super("Error! Restart application.");
}
}
