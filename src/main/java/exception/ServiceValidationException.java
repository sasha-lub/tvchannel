package exception;

/**
 * Created by Саня on 15.11.2016.
 */
public class ServiceValidationException extends RuntimeException {
    public ServiceValidationException(String message) {
        super(message);
    }
}
