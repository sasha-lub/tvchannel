package exception;

/**
 * Created by Саня on 10.11.2016.
 */
public class WrongPasswordException extends DomainLogicException {
    public WrongPasswordException(String login){
        super("Login error : wrong password for user : "+login);
    }
}
