package exception;

/**
 * Created by Саня on 10.11.2016.
 */
public class NoSuchEntityException extends ServiceValidationException {
    public NoSuchEntityException(Class clazz, Integer id){
        super("No such "+clazz.getName()+" entity by id "+id);
    }

    public NoSuchEntityException(Class clazz, String string){
        super("No such "+clazz.getName()+" entity for name "+string);
    }
}
