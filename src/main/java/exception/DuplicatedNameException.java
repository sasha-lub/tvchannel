package exception;

/**
 * Created by Саня on 11.11.2016.
 */
public class DuplicatedNameException extends DomainLogicException {
    public DuplicatedNameException(Class clazz, String name){
        super("Duplicated naming in class " + clazz + "for : "+name);
    }
}
