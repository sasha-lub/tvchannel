package exception;

/**
 * Created by Саня on 11.11.2016.
 */
public class WrongLoginException extends DomainLogicException {
    public WrongLoginException(String login){
        super("Login error! No user with login : "+login);
    }
}
