package exception;

/**
 * Created by Саня on 15.11.2016.
 */
public class DomainLogicException extends Exception{
    public DomainLogicException(String message){
        super(message);
    }
}
